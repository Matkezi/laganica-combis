#!/usr/bin/env bash

# wait for RabbitMQ server to start
sleep 10

#celery worker -A backend.settings.celery -Q default -n default@%h

celery -A backend.settings.celery worker -l info --concurrency=10
