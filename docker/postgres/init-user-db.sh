#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER smithx WITH PASSWORD 'smithx1234' CREATEDB;
    CREATE DATABASE smithx_dev;
    GRANT ALL PRIVILEGES ON DATABASE smithx_dev TO smithx;
EOSQL
