if(process.env.NODE_ENV == "production") {
   var REACT_APP_REGISTER_INSTAGRAM_ACCOUNT="https://api.instagram.com/oauth/authorize/?client_id=f917af346bb148db93bc4cfef462dc08&redirect_uri=http://94.130.58.239:9000/app/insta/login/callback&response_type=code&scope=public_content+comments+likes+follower_list+relationships+basic";
   var ROOT_URL = "http://94.130.58.239:9000/";

}
else {
  var REACT_APP_REGISTER_INSTAGRAM_ACCOUNT="https://api.instagram.com/oauth/authorize/?client_id=f917af346bb148db93bc4cfef462dc08&redirect_uri=http://localhost:9001/app/insta/login/callback&response_type=code&scope=public_content+comments+likes+follower_list+relationships+basic";
  var ROOT_URL = "http://localhost:9000/";

}

export const AuthUrls = {
    LOGIN: `${ROOT_URL}rest-auth/login/`,
    SIGNUP: `${ROOT_URL}rest-auth/registration/`,
    CHANGE_PASSWORD: `${ROOT_URL}rest-auth/password/change/`,
    RESET_PASSWORD: `${ROOT_URL}rest-auth/password/reset/`,
    RESET_PASSWORD_CONFIRM: `${ROOT_URL}rest-auth/password/reset/confirm/`,
    USER_ACTIVATION: `${ROOT_URL}rest-auth/registration/verify-email/`,
    USER_PROFILE: `${ROOT_URL}rest-auth/user/`,
    FB_LOGIN: `${ROOT_URL}rest-auth/facebook/`,
};


export const SmithxUrls = {
        REACT_APP_REGISTER_INSTAGRAM_ACCOUNT: `${REACT_APP_REGISTER_INSTAGRAM_ACCOUNT}`,
        HOME:  `${ROOT_URL}smithx/home`,
        INSTAGRAM_USER_DROPDOWN: `${ROOT_URL}smithx/instagram_user_dropwdown`,
        COMMENT: `${ROOT_URL}smithx/comment`,
        GET_FOLLOWING_LIST: `${ROOT_URL}smithx/following`,
        FOLLOW_USER: `${ROOT_URL}smithx/follow_user`,
        LIKE: `${ROOT_URL}smithx/like`,
        HOME_DETAILS: `${ROOT_URL}smithx/home_details`,
        HOME_DETAILS_GRAPH: `${ROOT_URL}smithx/home_details_graph`,
        HOME_USER_LIKED_MEDIA: `${ROOT_URL}smithx/home_user_liked_media`,
        CREATE_DATASET: `${ROOT_URL}smithx/create_dataset`,
        UPDATE_DATASET: `${ROOT_URL}smithx/update_dataset`,
        DELETE_DATASET: `${ROOT_URL}smithx/delete_dataset`,
        DATASETS_DROPDOWN: `${ROOT_URL}smithx/datasets_dropdown`,
        FIND_INFLUENCERS: `${ROOT_URL}smithx/find_influencers`,
        SEARCH_LEADS: `${ROOT_URL}smithx/search_leads`,
        REGISTER_INSTAGRAM_ACC: `${ROOT_URL}smithx/insta/login/callback`,
        DATASETS_BY_USER: `${ROOT_URL}smithx/datasets_by_user`,
        PUBLIC_DATASETS: `${ROOT_URL}smithx/datasets_public`,
        ASSIGN_DATASET: `${ROOT_URL}smithx/datasets_assign`,
        CHANNEL: `${ROOT_URL}smithx/channel`,
        FOLLOWING: `${ROOT_URL}smithx/following`,
        CREATE_FOLLOWING: `${ROOT_URL}smithx/create_following`,
        LIKING_FOLLOWING: `${ROOT_URL}smithx/liking_following`,

}

export const KrvJeZivot = {
  DONATIONS: `${ROOT_URL}smithx/donations`,
  DONORS: `${ROOT_URL}smithx/donors`,
  CREATE_DONOR : `${ROOT_URL}smithx/create-donor`,
  STOCK: `http://demo7019088.mockable.io/stock`,
  UPDATE_STOCK: `${ROOT_URL}smithx/check-stock`,  
  DONATION: `http://demo6023711.mockable.io/donation`,
  PENDING: `${ROOT_URL}smithx/pending`,
  DONATERESPONSE:  `${ROOT_URL}smithx/donate`,

}