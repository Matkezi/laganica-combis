import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function fetchHomeDetailsGraph(username) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({method: 'POST', url: SmithxUrls.HOME_DETAILS_GRAPH,
                headers: { 'Authorization': 'Token ' + token },
                data: { username: username }},
            dispatch({type: "FETCH_HOME_USER_GRAPH"}),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({type: "FETCH_HOME_USER_GRAPH_FULLFILLED", payload: response.data});
                dispatch(hideLoading());

            })
            .catch((err) => {
                dispatch({type: "FETCH_HOME_USER_GRAPH_REJECTED", payload: err});
                dispatch(hideLoading());

            });
    };
}


