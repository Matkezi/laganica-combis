import axios from "axios";
import { AuthTypes } from "../../constants/actionTypes";
import { SmithxUrls } from "../../constants/urls";
import {fetchUserPostsDetails} from "./home-user-posts-details";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { reducer as notifReducer, actions as notifActions, Notifs } from 'redux-notifications';
const { notifSend } = notifActions;
    export function fetchHome(current_user) {

        return function (dispatch) {
           const token = localStorage.getItem("token");


            axios.get(SmithxUrls.HOME, { headers: { authorization: 'Token ' + token } },
                dispatch({type: "FETCHING_INSTAGRAM_USER_DETAILS"}),
                dispatch(showLoading()),
            )
                .then((response) => {
                    console.log(current_user);
                    console.log(response.data.length);
                    if (response.data.length > 0) {
                        dispatch(fetchUserPostsDetails(response.data[current_user].id, response.data[current_user].username));
                }
                 dispatch({type: "FETCHING_INSTAGRAM_USER_DETAILS_FULFILLED", payload: response.data});
                 dispatch(hideLoading());

                })
                .catch((err) => {
                console.log(err);
                  dispatch(notifSend({
                    message: err.message,
                    kind: "warning",
                    dismissAfter: 6000
                }));
                    dispatch({type: "FETCHING_INSTAGRAM_USER_DETAILS_REJECTED", payload: err});
                    dispatch(hideLoading());

                });
        };
    }



