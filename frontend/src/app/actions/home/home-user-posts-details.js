import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { actions as notifActions } from 'redux-notifications';
const { notifSend } = notifActions;

    export function fetchUserPostsDetails(insta_user_id, username) {
            const token = localStorage.getItem("token");

        return function (dispatch) {

            axios({method: 'POST', url: SmithxUrls.HOME_DETAILS,
                 headers: { 'Authorization': 'Token ' + token },
                    data: { username: username,
                    insta_user_id: insta_user_id } },

                    dispatch({type: "FETCH_HOME_USER_POST_DETAILS"}),
                                dispatch(showLoading()))
                .then((response) => {
                    dispatch({type: "FETCH_HOME_USER_POST_DETAILS_FULFILLED", payload: response.data});
                    dispatch(hideLoading());

                })
                .catch((err) => {
                    dispatch({type: "FETCH_HOME_USER_POST_DETAILS_REJECTED", payload: err});
                    dispatch(hideLoading());
                    var msg = err.response.data;
                    var indexStart = msg.indexOf("You have exceeded");
                    var msg = msg.substring(indexStart, indexStart+178)
                    msg += " Try chaning to another instagram user."
                    dispatch(notifSend({
                        message: msg,
                        kind: "warning",
                        dismissAfter: 5000
                    }));
                });
        };
    }



