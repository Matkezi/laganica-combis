import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar';

    export function fetchUserRecentLiked(username) {
            const token = localStorage.getItem("token");

        return function (dispatch) {
        axios({method: 'POST', url: SmithxUrls.HOME_USER_LIKED_MEDIA,
                headers: { 'Authorization': 'Token ' + token },
                data: { username: username }},

                    dispatch({type: "FETCH_RECENT_LIKED_MEDIA"}),
            dispatch(showLoading()))
                .then((response) => {
                    dispatch({type: "FETCH_RECENT_LIKED_MEDIA_FULLFILLED", payload: response.data});
                    dispatch(hideLoading());
                })
                .catch((err) => {
                    dispatch({type: "FETCH_RECENT_LIKED_MEDIA_REJECTED", payload: err});
                    dispatch(hideLoading());

                });
        };
    }


