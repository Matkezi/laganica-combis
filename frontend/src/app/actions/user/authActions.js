import axios from "axios";
import { SubmissionError } from 'redux-form';
import history from "../../utils/historyUtils";
import { actions as notifActions } from 'redux-notifications';
const { notifSend, notifDismiss } = notifActions;
import { AuthTypes } from "../../constants/actionTypes";
import { AuthUrls } from "../../constants/urls";
import { getUserToken } from "../../utils/authUtils";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { createFollowing } from "../shared/createFollowing"

export function authLogin(token) {
    return {
        type: AuthTypes.LOGIN,
        payload: token
    };
}

export function fbLogin(fbAuthToken) {
    return function (dispatch) {
        axios({
            method: 'POST', url: AuthUrls.FB_LOGIN,
            data: {
                access_token: fbAuthToken,
            }
        }, dispatch(notifSend({
            message: "Logging in..",
            kind: "info",
            id: "67",
        })))
            .then((response) => {
                dispatch(notifDismiss("67"));
                // If request is good...
                // Update state to indicate user is authenticated
                const token = response.data.key;
                dispatch(authLogin(token));

                localStorage.setItem("token", token);
                // redirect to the route '/'
                history.push("/app/home");
            })
            .catch((err) => {
                const processedError = processServerError(error.response.data);
                throw new SubmissionError(processedError);

            });
    };
}


export function loginUser(formValues, dispatch, props) {
    const loginUrl = AuthUrls.LOGIN;

    return axios.post(loginUrl, formValues).then((response) => {
        // If request is good...
        // Update state to indicate user is authenticated
        const token = response.data.key;
        dispatch(authLogin(token));

        localStorage.setItem("token", token);
        // redirect to the route '/'
        history.push("/app/home");
    }).catch(error => {
        const processedError = processServerError(error.response.data);
        throw new SubmissionError(processedError);
    });
}

export function logoutUser() {
    localStorage.removeItem("token");
    localStorage.removeItem("current_instagram_id");
    localStorage.removeItem("current_instagram_profile_picture");
    localStorage.removeItem("current_instagram_username");
    localStorage.removeItem("home_current__user");
    localStorage.removeItem("token");

    history.push("/app/login");

    return {
        type: AuthTypes.LOGOUT
    };

}

export function signupUser(formValues, dispatch, props) {
    const signupUrl = AuthUrls.SIGNUP;
    dispatch(notifDismiss("66"));

    return axios(
        {
            method: 'POST', url: signupUrl,
            data: formValues
        },
        dispatch(notifSend({
            message: "Trying to sign you up, please wait a moment. :)",
            kind: "info",
            id: "66",
        }))
    )

        .then((response) => {
            dispatch(notifDismiss("66")),

                dispatch(notifSend({
                    message: "Signup success! Sending you confirmation email...",
                    kind: "success",
                    dismissAfter: 3000
                }));
            history.push("/app/signup_done");
        })
        .catch((error) => {
            // If request is bad...
            // Show an error to the user
            const processedError = processServerError(error.response.data);
            throw new SubmissionError(processedError);
        });
}

function setUserProfile(payload) {
    return {
        type: AuthTypes.USER_PROFILE,
        payload: payload
    };
}

export function getUserProfile() {
    return function (dispatch) {
        {/* const token = getUserToken(store.getState()); */ }
        const token = localStorage.getItem("token");

        if (token) {
            axios.get(AuthUrls.USER_PROFILE, {
                headers: {
                    authorization: 'Token ' + token
                }
            }).then(response => {
                dispatch(setUserProfile(response.data))
            }).catch((error) => {
                // If request is bad...
                // Show an error to the user
                console.log(error);
                // TODO: send notification and redirect
            });
        }
    };
}

export function changePassword(formValues, dispatch, props) {
    const changePasswordUrl = AuthUrls.CHANGE_PASSWORD;
    const token = localStorage.getItem("token");

    if (token) {
        return axios.post(changePasswordUrl, formValues, {
            headers: {
                authorization: 'Token ' + token
            }
        })
            .then((response) => {
                dispatch(notifSend({
                    message: "Password has been changed successfully",
                    kind: "info",
                    dismissAfter: 5000
                }));
                // redirect to the route '/profile'
                history.push("/app/home");
            })
            .catch((error) => {
                // If request is bad...
                // Show an error to the user
                const processedError = processServerError(error.response.data);
                throw new SubmissionError(processedError);
            });
    }
}

export function resetPassword(formValues, dispatch, props) {
    const resetPasswordUrl = AuthUrls.RESET_PASSWORD;

    return axios.post(resetPasswordUrl, formValues)
        .then(response => {
            // redirect to reset done page
            history.push("/app/reset_password_done");
        }).catch((error) => {
            // If request is bad...
            // Show an error to the user
            const processedError = processServerError(error.response.data);
            throw new SubmissionError(processedError);
        });
}

export function confirmPasswordChange(formValues, dispatch, props) {
    const { uid, token } = props.match.params;
    const resetPasswordConfirmUrl = AuthUrls.RESET_PASSWORD_CONFIRM;
    const data = Object.assign(formValues, { uid, token });

    return axios.post(resetPasswordConfirmUrl, data)
        .then(response => {
            dispatch(notifSend({
                message: "Password has been reset successfully, please log in",
                kind: "info",
                dismissAfter: 5000
            }));

            history.push("/app/login");
        }).catch((error) => {
            // If request is bad...
            // Show an error to the user
            const processedError = processServerError(error.response.data);
            throw new SubmissionError(processedError);
        });
}

export function activateUserAccount(formValues, dispatch, props) {
    const { key } = props.match.params;
    const activateUserUrl = AuthUrls.USER_ACTIVATION;
    const data = Object.assign(formValues, { key });

    return axios.post(activateUserUrl, data)
        .then(response => {
            dispatch(notifSend({
                message: "Your account has been activated successfully, please log in",
                kind: "info",
                dismissAfter: 5000
            }));

            history.push("/app/login");
        }).catch((error) => {
            // If request is bad...
            // Show an error to the user
            const processedError = processServerError(error.response.data);
            throw new SubmissionError(processedError);
        });
}

export function updateUserProfile(formValues, dispatch, props) {
    const token = localStorage.getItem("token");

    return axios.patch(AuthUrls.USER_PROFILE, formValues, {
        headers: {
            authorization: 'Token ' + token
        }
    })
        .then(response => {
            dispatch(notifSend({
                message: "Your profile has been updated successfully",
                kind: "info",
                dismissAfter: 5000
            }));

            history.push("/app/profile");
        }).catch((error) => {
            // If request is bad...
            // Show an error to the user
            const processedError = processServerError(error.response.data);
            throw new SubmissionError(processedError);
        });
}
// util functions
function processServerError(error) {
    return Object.keys(error).reduce(function (newDict, key) {
        if (key === "non_field_errors") {
            newDict["_error"].push(error[key]);
        } else if (key === "token") {
            // token sent with request is invalid
            newDict["_error"].push("The link is not valid any more.");
        } else {
            newDict[key] = error[key];
        }

        return newDict
    }, { "_error": [] });
}