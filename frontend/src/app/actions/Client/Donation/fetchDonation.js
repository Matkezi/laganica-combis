import axios from "axios";
import { KrvJeZivot } from "../../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function fetchDonation(username, id) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({
            method: 'POST', url: KrvJeZivot.DONATION + "/" + id,
            headers: { 'Authorization': 'Token ' + token },
            data: { username: username, id: id }
        },
            dispatch({ type: "FETCH_DONATION" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_DONATION_FULLFILLED", payload: response.data });
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "FETCH_DONATION_REJECTED", payload: err });
                dispatch(hideLoading());

            });
    };
}


