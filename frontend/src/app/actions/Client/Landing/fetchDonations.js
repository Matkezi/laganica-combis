import axios from "axios";
import { KrvJeZivot } from "../../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function fetchDonations(username) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({
            method: 'GET', url: KrvJeZivot.DONATIONS + "/5",
            headers: { 'Authorization': 'Token ' + token },
        },
            dispatch({ type: "FETCH_DONATIONS" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_DONATIONS_FULLFILLED", payload: response.data });
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "FETCH_DONATIONS_REJECTED", payload: err });
                dispatch(hideLoading());

            });
    };
}


