import axios from "axios";
import { KrvJeZivot } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import history from "../../utils/historyUtils";

export function donateResponse(username, answer) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({
            method: 'POST', url: KrvJeZivot.DONATERESPONSE   ,
            headers: { 'Authorization': 'Token ' + token },
            data: { username: username, answer: answer }

        },
            dispatch({ type: "FETCH_DONATE_RESPONSE" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_DONATE_RESPONSE_FULLFILLED", payload: response.data });
                history.push("/app/");
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "FETCH_DONATE_RESPONSE_REJECTED", payload: err });

                dispatch(hideLoading());
                history.push("/app/");


            });
    };
}


