import axios from "axios";
import { KrvJeZivot } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function fetchDonors() {
    const token = localStorage.getItem("token");

    return function (dispatch) {

        axios.get(KrvJeZivot.DONORS, {
            headers: { authorization: 'Token ' + token }
        },
            dispatch({ type: "FETCHING_DONORS" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_DONORS_FULLFILED", payload: response.data });
                dispatch(hideLoading());

            })
            .catch((err) => {
                dispatch({ type: "FETCH_DONORS_REJECTED", payload: err });
                dispatch(hideLoading());

            });
    };
}



