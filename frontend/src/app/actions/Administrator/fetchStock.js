import axios from "axios";
import { KrvJeZivot } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function fetchStock() {
    const token = localStorage.getItem("token");

    return function (dispatch) {

        axios.get(KrvJeZivot.STOCK, {
            headers: { authorization: 'Token ' + token }
        },
            dispatch({ type: "FETCHING_STOCK" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_STOCK_FULLFILED", payload: response.data });
                dispatch(hideLoading());

            })
            .catch((err) => {
                dispatch({ type: "FETCH_STOCK_REJECTED", payload: err });
                dispatch(hideLoading());

            });
    };
}



