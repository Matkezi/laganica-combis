import axios from "axios";
import { KrvJeZivot } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function updateStock() {
    const token = localStorage.getItem("token");

    return function (dispatch) {

        axios.get(KrvJeZivot.UPDATE_STOCK, {
            headers: { authorization: 'Token ' + token }
        },
            dispatch({ type: "FETCHING_UPDATA_STOCK" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "UPDATA_STOCK_FULLFILED", payload: response.data });
                dispatch(hideLoading());

            })
            .catch((err) => {
                dispatch({ type: "UPDATA_STOCK_REJECTED", payload: err });
                dispatch(hideLoading());

            });
    };
}



