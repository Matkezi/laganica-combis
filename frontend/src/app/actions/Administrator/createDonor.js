import axios from "axios";
import { KrvJeZivot } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { actions as notifActions } from 'redux-notifications';
const { notifSend } = notifActions;
import history from "../../utils/historyUtils";

export function createDonor(first_name) {
    return function (dispatch) {
        const token = localStorage.getItem("token");
        axios({
            method: 'POST', url: KrvJeZivot.CREATE_DONOR,
            headers: { 'Authorization': 'Token ' + token },
            data: {
                first_name: first_name
            }
        },
            dispatch({ type: "DONOR_CREATING" }),
            dispatch(showLoading()),
        )
            .then((response) => {
                dispatch({ type: "DONOR_CREATE_FULFILLED", payload: response.data });
                dispatch(hideLoading());
                setTimeout(function () { history.push("/app"); }.bind(this), 700);

            })
            .catch((err) => {
                dispatch({ type: "DONOR_CREATE_REJECTED", payload: err });
                dispatch(hideLoading());
                dispatch(notifSend({
                    message: err.response.data,
                    kind: "warning",
                    dismissAfter: 10000
                }));
            });
    };
}



