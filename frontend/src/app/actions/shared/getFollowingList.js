import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function getFollowingList(username) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({
            method: 'GET', url: SmithxUrls.GET_FOLLOWING_LIST + "?username=" + username,
            headers: { 'Authorization': 'Token ' + token }
        },

            dispatch({ type: "FETCH_FOLLOWING_LIST" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FETCH_FOLLOWING_LIST_FULFILLED", payload: response.data });
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "FETCH_FOLLOWING_LIST_REJECTED", payload: err });
                dispatch(hideLoading());
            });
    };
}



