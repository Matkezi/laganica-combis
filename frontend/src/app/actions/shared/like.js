import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

    export function likePost(username, media_id) {
            const token = localStorage.getItem("token");

        return function (dispatch) {
        axios({method: 'POST', url: SmithxUrls.LIKE,
                headers: { 'Authorization': 'Token ' + token },
                data: { username: username, media_id: media_id }},

                dispatch({type: "LIKING"}),
            dispatch(showLoading()))
                .then((response) => {
                    dispatch({type: "LIKING_FULFILLED", payload: response.data});
                    dispatch(hideLoading());
                })
                .catch((err) => {
                    dispatch({type: "LIKING_REJECTED", payload: err});
                    dispatch(hideLoading());
                });
        };
    }



