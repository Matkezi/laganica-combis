import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

    export function fetchInstagramUserDropdown() {
    const token = localStorage.getItem("token");


        return function (dispatch) {
            axios.get(SmithxUrls.INSTAGRAM_USER_DROPDOWN, { headers: { authorization: 'Token ' + token } },
                dispatch(showLoading()))
                .then((response) => {
                    dispatch({type: "FETCH_INSTAGRAM_USER_DROPDOWN_FULFILLED", payload: response.data});
                    dispatch(hideLoading());
                })
                .catch((err) => {
                    dispatch({type: "FETCH_INSTAGRAM_USER_DROPDOWN_REJECTED", payload: err});
                    dispatch(hideLoading());
                });
        };
    }

