import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function follow_user(username, user_to_follow) {
    const token = localStorage.getItem("token");

    return function (dispatch) {
        axios({
            method: 'POST', url: SmithxUrls.FOLLOW_USER,
            headers: { 'Authorization': 'Token ' + token },
            data: {
                username: username,
                user_to_follow: user_to_follow
            }
        },

            dispatch({ type: "FOLLOWING" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "FOLLOWING_FULFILLED", payload: response.data });
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "FOLLOWING_REJECTED", payload: err });
                dispatch(hideLoading());
            });
    };
}



