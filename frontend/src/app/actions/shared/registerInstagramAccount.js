import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { fetchInstagramUserDropdown } from "./InstagramUserDropdown";
import { createFollowing } from "./createFollowing"
const { notifSend } = notifActions;
import { actions as notifActions } from 'redux-notifications';
import history from "../../utils/historyUtils";

export function register_instagram_account(code) {
    const token = localStorage.getItem("token");

    return function (dispatch) {

        axios({
            method: 'POST', url: SmithxUrls.REGISTER_INSTAGRAM_ACC,
            headers: { 'Authorization': 'Token ' + token },
            data: { code: code }
        },

            dispatch({ type: "REGISTERING" }),
            dispatch(showLoading()))

            .then((response) => {
                dispatch({ type: "REGISTERING_FULFILLED", payload: response.data });
                dispatch(notifSend({
                    message: "Account added! Check Instagram accounts dropdown in header of the app.",
                    kind: "success",
                    dismissAfter: 6000
                }));
                dispatch(fetchInstagramUserDropdown())
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "REGISTERING_REJECTED", payload: err });
                dispatch(hideLoading());
            });
    };
}



