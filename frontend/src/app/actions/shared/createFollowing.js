import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

export function createFollowing() {
    const token = localStorage.getItem("token");
    const username = localStorage.getItem("current_instagram_username");
    const insta_user_id = localStorage.getItem("current_instagram_id");
    return function (dispatch) {
        axios({
            method: 'POST', url: SmithxUrls.CREATE_FOLLOWING,
            headers: { 'Authorization': 'Token ' + token },
            data: {
                username: username,
                insta_user_id: insta_user_id
            }
        },

            dispatch({ type: "CREATE_FOLLOWING" }),
            dispatch(showLoading()))
            .then((response) => {
                dispatch({ type: "CREATE_FOLLOWING_FULFILLED", payload: response.status });
                dispatch(hideLoading());
            })
            .catch((err) => {
                dispatch({ type: "CREATE_FOLLOWING_REJECTED", payload: err });
                dispatch(hideLoading());
            });
    };
}



