import axios from "axios";
import { SmithxUrls } from "../../constants/urls";
import { showLoading, hideLoading } from 'react-redux-loading-bar'

    export function commentAdd(username, comment, media_id) {
            const token = localStorage.getItem("token");

        return function (dispatch) {
                axios({method: 'POST', url: SmithxUrls.COMMENT,
                headers: { 'Authorization': 'Token ' + token },
                data: { username: username,
                 comment: comment,
                 media_id: media_id }},

                dispatch({type: "COMMENTING"}),
                    dispatch(showLoading()))
                .then((response) => {
                    dispatch({type: "COMMENT_FULFILLED", payload: response.data});
                    dispatch(hideLoading());
                })
                .catch((err) => {
                    dispatch({type: "COMMENT_REJECTED", payload: err});
                    dispatch(hideLoading());
                });
        };
    }



