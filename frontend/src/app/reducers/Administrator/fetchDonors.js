const initialState = {
    donors: {},
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCHING_DONORS": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_DONORS_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_DONORS_FULLFILED": {
            return {...state,
                fetching: false,
                fetched: true,
                publicDataset: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;