const initialState = {
    donor: {},
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "DONOR_CREATING": {
            return {...state, fetching: true, fetched: false};
        }
        case "DONOR_CREATE_FULFILLED_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "DONOR_CREATE_FULFILLED_FULLFILED": {
            return {...state,
                fetching: false,
                fetched: true,
                donor: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;