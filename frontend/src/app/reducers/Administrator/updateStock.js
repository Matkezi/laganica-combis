const initialState = {
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCHING_UPDATA_STOCK": {
            return {...state, fetching: true, fetched: false};
        }
        case "UPDATA_STOCK_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "UPDATA_STOCK_FULLFILED": {
            return {...state,
                fetching: false,
                fetched: true
            };
        }
        default: return state;

    }

}
export default reducer;