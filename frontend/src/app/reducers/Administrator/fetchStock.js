const initialState = {
    stock: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCHING_STOCK": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_STOCK_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_STOCK_FULLFILED": {
            return {...state,
                fetching: false,
                fetched: true,
                stock: action.payload.data
            };
        }
        default: return state;

    }

}
export default reducer;