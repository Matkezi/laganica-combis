const initialState = {
    donation_list: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_DONATIONS": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_DONATIONS_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_DONATIONS_FULLFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                donation_list: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;