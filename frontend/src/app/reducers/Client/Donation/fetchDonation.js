const initialState = {
    donation: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_DONATION": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_DONATION_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_DONATION_FULLFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                donation: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;