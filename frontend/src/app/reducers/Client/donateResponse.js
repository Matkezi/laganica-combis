const initialState = {
    donateResponse: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_DONATE_RESPONSE": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_DONATE_RESPONSE_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_DONATE_RESPONSE_FULLFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                donateResponse: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;