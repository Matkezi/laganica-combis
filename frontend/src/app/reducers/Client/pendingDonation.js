const initialState = {
    pending: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_PENDING": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_PENDING_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_PENDING_FULLFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                pending: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;