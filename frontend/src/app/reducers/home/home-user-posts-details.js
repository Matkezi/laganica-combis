const initialState = {
    user_post_details: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_HOME_USER_POST_DETAILS": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_HOME_USER_POST_DETAILS_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_HOME_USER_POST_DETAILS_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                user_post_details: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;