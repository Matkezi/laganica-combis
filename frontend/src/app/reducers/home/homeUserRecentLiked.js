const initialState = {
    recent_liked: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_RECENT_LIKED_MEDIA": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_RECENT_LIKED_MEDIA_FULLFILLED": {
            return {...state, fetching: false, recent_liked: action.payload};
        }
        case "FETCH_RECENT_LIKED_MEDIA_REJECTED": {
            return {...state,
                fetching: false,
                fetched: true,
                error: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;