const initialState = {
    user_info: [],
    fetching: false,
    fetched: false,
    error: null
};
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCHING_INSTAGRAM_USER_DETAILS": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCHING_INSTAGRAM_USER_DETAILS_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCHING_INSTAGRAM_USER_DETAILS_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                user_info: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;