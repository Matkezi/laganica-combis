const initialState = {
    graph_data: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_HOME_USER_GRAPH": {
            return {...state, fetching: true, fetched: false};
        }
        case "FETCH_HOME_USER_GRAPH_FULLFILLED": {
            return {...state, fetching: false, graph_data: action.payload};
        }
        case "FETCH_HOME_USER_GRAPH_REJECTED": {
            return {...state,
                fetching: false,
                fetched: true,
                error: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;