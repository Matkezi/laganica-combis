const initialState = {
    comment: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "COMMENTING": {
            return {...state, fetching: true};
        }
        case "COMMENT_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "COMMENT_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                comment: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;