const initialState = {
    following_list: [],
    fetching: false,
    fetched: false,
    error: null
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_FOLLOWING_LIST": {
            return {...state, fetching: true};
        }
        case "FETCH_FOLLOWING_LIST_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_FOLLOWING_LIST_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                following_list: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;