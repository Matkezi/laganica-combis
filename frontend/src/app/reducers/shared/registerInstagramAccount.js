const initialState = {
    register: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "REGISTERING": {
            return {...state, fetching: true};
        }
        case "REGISTERING_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "REGISTERING_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                register: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;