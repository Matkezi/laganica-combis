const initialState = {
    like: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "LIKING": {
            return {...state, fetching: true};
        }
        case "LIKING_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "LIKING_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                like: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;