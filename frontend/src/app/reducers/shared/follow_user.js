const initialState = {
    follow_user: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FOLLOWING": {
            return {...state, fetching: true};
        }
        case "FOLLOWING_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FOLLOWING_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                follow_user: action.payload
            };
        }
        default: return state;

    }

}
export default reducer;