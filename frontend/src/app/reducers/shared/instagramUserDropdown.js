const initialState = {
    instagram_user_dropdown: [],
    fetching: false,
    fetched: false,
    error: null
}
function reducer(state = initialState, action) {
    switch (action.type) {
        case "FETCH_INSTAGRAM_USER_DROPDOWN": {
            return {...state, fetching: true};
        }
        case "FETCH_INSTAGRAM_USER_DROPDOWN_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "FETCH_INSTAGRAM_USER_DROPDOWN_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                instagram_user_dropdown: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;