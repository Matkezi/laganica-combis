const initialState = {
    status: "",
    fetching: false,
    fetched: false,
    error: null
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case "CREATE_FOLLOWING": {
            return {...state, fetching: true};
        }
        case "CREATE_FOLLOWING_REJECTED": {
            return {...state, fetching: false, error: action.payload};
        }
        case "CREATE_FOLLOWING_FULFILLED": {
            return {...state,
                fetching: false,
                fetched: true,
                status: action.payload
            };
        }
        default: return state;

    }

}

export default reducer;