import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Moment from 'react-moment';
import 'moment-timezone';
import DataTile from "../../ReusableComponents/DataTile";

const queryString = require('query-string');
import axios from "axios";
import { connect } from "react-redux";


class Contents extends React.Component {

    constructor() {
        super();
        this.state = {
            width: "6",
            theme: "dataset-tile from-public-tile",
            number: "",
            icon: "fa fa-database",
            subject: "Assign new dataset from Dataset Store",
            link: "/app/dataset-store",
            link_text: "Go to dataset store",
            width2: "6",
            theme2: "bg-yellow",
            number2: "+",
            icon2: "fa fa-database",
            subject2: "Can't find what you need? Create new Dataset to your liking!",
            link2: "/app/create-dataset",
            link_text2: "Create dataset",
        }
    }

    remote(remoteObj) {
        // Only cell editing, insert and delete row will be handled by remote store
        remoteObj.cellEdit = false;
        remoteObj.insertRow = false;
        remoteObj.dropRow = false;
        return remoteObj;
    }

    formatBeginTime(cell, row) {
        return (
            <Moment format="DD.MM.YYYY. HH:mm">{row.collectionBeginTime}</Moment>
        );
    }

    formatEndTime(cell, row) {
        return (
            <Moment format="DD.MM.YYYY. HH:mm">{row.collectionEndTime}</Moment>
        );
    }


    asignDataset(cell, row) {

        return (
            <div>
                <button onClick={() => {
                    this.props.dispatch(datasetAssign(row.id));
                }}
                    className="btn table-action-button col-lg-12">
                    Take it
                </button>
            </div>
        );
    }


    render() {
        const props = this.props;

        const selectRow = {
            mode: "checkbox",
            clickToSelct: true
        };

        return (

            <div>
                <div className={"col-lg-12 col-sm-12 col-xs-12 col-md-12"}>
                    <div className={"small-box"} id="public-dataset">
                        <div className="inner">
                            <div className="row" style={{ padding: "30px" }}>
                                <h4><i className="fa fa-folder" />&nbsp;Donors</h4>


                                <BootstrapTable data={this.props.data}
                                    remote={this.remote} striped hover
                                    search searchPlaceholder="Search donors" pagination>
                                    <TableHeaderColumn isKey={true} dataField='first_name'
                                        width='15%'>Name</TableHeaderColumn>
                                    <TableHeaderColumn dataField='location' dataAlign='center'
                                        headerAlign='center'
                                        width='15%'
                                    >Location</TableHeaderColumn>
                                </BootstrapTable>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

@connect((store) => {
    return {


    };
})
export class DonorsTable extends React.Component {

    render() {
        const props = this.props;

        return (
            <div className="row">
                <Contents {...props} />
            </div>
        );
    }
};

export default DonorsTable;