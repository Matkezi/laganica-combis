import * as React from "react";
import { connect } from "react-redux";
import { fetchDonors } from "../../../actions/Administrator/fetchDonors";
import DonorsTable from "./donors";
import { Link, Route } from "react-router-dom";


class Contents extends React.Component {

    constructor() {
        super();
        this.state = {
            attributes: "",
            username: localStorage.getItem("current_instagram_username")
        }
    }

    componentWillMount() {
        this.props.dispatch(fetchDonors());
    }


    render() {
        const props = this.props;

        return (

            <div className="col-lg-12">
                <DonorsTable datasetDetails={this.props.donors}
                    fetched={this.props.fetchedDonors}
                    fetching={this.props.fetchingDonors} />
                <Link to="/app/create-area"><button className="material-add-button"><i className="fa fa-plus plusIconButton" /></button></Link>
            </div>

        );
    }
};

@connect((store) => {
    return {
        donors: store.fetchDonors.donors,
        fetchedDonors: store.fetchDonors.fetched,
        fetchingDonors: store.fetchDonors.fetching

    };
})
export class Donors extends React.Component {
    render() {
        const props = this.props;

        return (
            <div className="row">
                <Contents {...props} />
            </div>
        );
    }
}
;

export default Donors;