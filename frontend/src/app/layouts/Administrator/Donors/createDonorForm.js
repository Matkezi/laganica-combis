import React from "react";
import ReactDOM from "react-dom";
import DjangoCSRFToken from "django-react-csrftoken";
import { createDonor } from "../../../actions/Administrator/createDonor";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import moment from "moment";
import { Well, Breadcrumb } from "react-bootstrap";
import { Link, Route } from "react-router-dom";


@connect()
class Contents extends React.Component {

    constructor() {
        super();
        this.state = {
            first_name: "",
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);

    }

    onSubmit(e) {
        e.preventDefault();
        const { first_name } = this.state;
        this.props.dispatch(createDonor(first_name));
    }

    onChangeInput(ev) {
        this.setState({ [ev.target.name]: ev.target.value });
    }

    render() {
        const props = this.props;

        return (
            <div className="">

                <Breadcrumb>
                    <Breadcrumb.Item active><i>Create new Donor</i></Breadcrumb.Item>
                </Breadcrumb>
                <div className="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                    <form onSubmit={this.onSubmit} className="form-horizontal">
                        <fieldset>

                            <div className="form-group">
                                <div className="col-md-12">
                                    <label className="col-form-label">First Name:</label>
                                    <input type="text" name="first_name" value={name}
                                        onChange={this.onChangeInput} placeholder="First Name"
                                        className="form-control"
                                        required />
                                </div>
                            </div>


                            <button type="submit" className="btn btn-automatedLiking col-md-12 col-sm-12 col-lg-12 col-xs-12">Create new Donor
    </button>


                        </fieldset>
                    </form>

                </div>
            </div>
        );
    }
}


export class CreateDonorForm extends React.Component {
    render() {
        const props = this.props;
        return (
            <div className="row">
                <Contents {...props} />
            </div >


        );
    }
}
;

export default CreateDonorForm;
