import React, { Component } from "react";
import { BarChart, Bar, XAxis, YAxis, Cell, CartesianGrid, Tooltip, Legend } from "recharts"


class CustomizedLabel extends Component {

    render() {
        const { x, y, fill, value } = this.props;
        return <text
            x={x}
            y={y}
            dy={-8}
            dx={50}
            fontSize='16'
            fontFamily='sans-serif'
            fill={fill}
            textAnchor="middle">{value}%</text>
    }
};

export default class StockBar extends Component {

    render() {
        const { data } = this.props;
        return (
            <div>
                <BarChart
                    width={900}
                    height={260}
                    data={data}
                    margin={{ top: 5, right: 0, left: 0, bottom: 25 }}>
                    <XAxis
                        dataKey="blood_type"
                        fontFamily="sans-serif"
                        tickSize
                        dy='25'
                    />
                    <YAxis hide />
                    <CartesianGrid
                        vertical={false}
                        stroke="#ebf3f0"
                    />
                    <Bar
                        dataKey="state"
                        barSize={170}
                        fontFamily="sans-serif"
                        label={<CustomizedLabel />}
                    >
                        {
                            data.map((entry, index) => {
                                if (data[index].alert === "green") {
                                    return (<Cell fill={'#197319'} />)
                                }
                                else if (data[index].alert === "yellow") {
                                    return (<Cell fill={'#e7da27'} />)
                                }
                                else if (data[index].alert === "red") {
                                    return (<Cell fill={'#cc0000'} />)
                                }
                            })
                        }
                    </Bar>
                </BarChart>
            </div>
        );
    }


}