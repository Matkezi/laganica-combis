import * as React from "react";
import { connect } from "react-redux";
import { fetchStock } from "../../../actions/Administrator/fetchStock";
import { Link, Route } from "react-router-dom";
import StockBar from "./stock"
import SmartManager from "./smartManager"

class Contents extends React.Component {

    constructor() {
        super();
        this.state = {
            attributes: "",
            username: localStorage.getItem("current_instagram_username")
        }
    }

    componentWillMount() {
        this.props.dispatch(fetchStock());
    }


    render() {
        const props = this.props;

        return (

            <div className="container">
            <br/> <br/> <br/>
                <StockBar data={this.props.stock} />
                <SmartManager />
                <Link to="/app/create-area"><button className="material-add-button"><i className="fa fa-plus plusIconButton" /></button></Link>
            </div>

        );
    }
};

@connect((store) => {
    return {
        stock: store.fetchStock.stock
    };
})
export class Stock extends React.Component {
    render() {
        const props = this.props;
        return (
            <div className="row">
                <Contents {...props} />
            </div>
        );
    }
}
;

export default Stock;