import React, { Component } from "react";
import { connect } from "react-redux";
import { updateStock } from "../../../actions/Administrator/updateStock";
import { renderToggle } from "../../../utils/renderUtils"
import { reduxForm, Field, propTypes, FieldArray } from "redux-form";
import { Button} from "react-bootstrap"


@connect()
export class SmartManager extends React.Component {

    constructor(props, context) {
        super(props, context);
    
        this.handleClick = this.handleClick.bind(this);
    
        this.state = {
          isLoading: false
        };
      }

    handleClick() {
        this.setState({ isLoading: true });
        this.props.dispatch(updateStock());
        // This probably where you would have an `ajax` call
        setTimeout(() => {
          // Completed of async action, set loading state back
          this.setState({ isLoading: false });
        }, 2000);
      }
    render() {
        const { isLoading } = this.state;
        return (
            <div className="container">
            <div className="col-lg-6">
                <form>
                    <fieldset className="form-group">
                        <label>Allow Smart Manager to call for donations automatically</label>
                        <Field name="publish_user_profile" component={renderToggle}
                            type="checkbox"
                        />
                    </fieldset>
                </form>
                </div>
                <div className="col-lg-6">

                <Button bsStyle="danger" className="col-lg-12"
                disabled={isLoading}
                onClick={!isLoading ? this.handleClick : null}>
                Call for donations</Button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(reduxForm({
    form: "update_user_profile",
    onSubmit: updateStock
})(SmartManager));
