import { SmithxUrls } from "../../../constants/urls"
import { Modal, Button } from 'react-bootstrap'
import React from "react";


export default class RegisterInstaUserTile extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            show: false
        }
    }

     static defaultProps = {
        width: "12",
        theme: "bg-yellow",
        icon: "fa fa-instagram",
        subject: "Registered Instagram Users",
        stats: "#",
        link: SmithxUrls.REACT_APP_REGISTER_INSTAGRAM_ACCOUNT,
     };

    handleHide() {
        this.setState({ show: false });
    }

    render() {

        var link = "",
            stats = <h3> {this.props.numberOfAccounts} </h3>;

        if (this.props.link) {
            link =
                <div className="small-box-footer">
                    Register New User <i className="fa fa-id-card"></i>
                </div>;
        }

        if (this.props.stats.indexOf("%") !== -1) {
            var style = {
                fontSize: "20px"
            };

            stats =
                <h3>
                    {this.props.stats.replace(/%/g, "")}
                    <sup style={style}>%</sup>
                </h3>;
        }

        return (
            <div>
                <a style={{ cursor: 'pointer' }} onClick={() => this.setState({ show: true })} >

                    <div className={"small-box " + this.props.theme}>
                        <div className="inner">
                            {stats}
                            <p>{this.props.subject}</p>
                        </div>
                        <div className="icon">
                            <i className={"fa " + this.props.icon}></i>
                        </div>
                        {link}
                    </div>
                </a>
                <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            Adding new Instagram account
            </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Before adding new Instagram account, please go to <a target="_blank" href="http://www.instagram.com">Instagram</a> and make sure that you are <b>LOGGED OUT</b> from your account.
                                <br />

                    </Modal.Body>
                    <Modal.Footer>

                        <a href={this.props.link}> <Button className="btn-automatedLiking" onClick={this.handleHide}>Ok, I am logged out, let's do this!</Button> </a>
                    </Modal.Footer>
                </Modal>
            </div>

        );
    }
}