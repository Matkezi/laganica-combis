import React from "react";
import { connect } from "react-redux";

import { isEmpty } from "lodash";
import {fetchUserPostsDetails} from "../../../actions/home/home-user-posts-details";


@connect((store) => {
    return {
        user_post_details: store.home_user_posts_details.user_post_details,
        fetched: store.home_user_posts_details.fetched,
        fetching: store.home_user_posts_details.fetching,
    };
})
class ProfileCardRefactor extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = { selectedUserIndex: 2 };

    }


    handleClick(event){
        event.preventDefault();
        this.props.dispatch(fetchUserPostsDetails(this.props.id, this.props.username));
        this.props.onUserChange(this.props.userIndex);


    }


    render() {

        return (
            <div className="col-lg-6 col-sm-12 col-md-12" style={{padding: '0px'}}>
                    <div className="card kl-card kl-xl kl-shine kl-show">

                        <div className="kl-card-block kl-xl bg-primary kl-shadow-b kl-overlay kl-slide-in kl-show">
                            <div className="kl-card-overlay kl-card-overlay-split-q kl-dark kl-inverse">
                                <div className="kl-card-overlay-item">

                                </div>
                                <div className="kl-card-overlay-item"></div>
                                <div className="kl-card-overlay-item">
                                    <div className="kl-figure-block">
                                        <br />
                                        <span className="kl-figure kl-txt-shadow">{this.props.followers}</span>
                                        <span className="kl-title">Followers</span>
                                    </div>
                                </div>
                                <div className="kl-card-overlay-item">
                                    <div className="kl-figure-block">
                                        <br />
                                        <span className="kl-figure">{this.props.following}</span>
                                        <span className="kl-title">Following</span>
                                    </div>
                                </div>
                            </div>

                            <div className="card-block">
                                <h4 className="card-title text-center">{this.props.displayName}</h4>
                                <p className="card-text text-center">{this.props.bio}</p>

                                <div className="kl-card-item kl-ptr kl-show mr-1 mt-1"><span className="badge badge-success">Show posts</span></div>
                            </div>

                            <a href="#" onClick={this.handleClick} className="kl-card-avatar kl-xl kl-pm"><img className="kl-b-danger kl-b-circle kl-b-5 kl-slow kl-shadow-br kl-reveal kl-shine" src={this.props.displayPicture} /></a>

                        </div>

                </div>

               </div>
        );
    }


}

export default ProfileCardRefactor;

