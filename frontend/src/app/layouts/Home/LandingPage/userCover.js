import React from "react";

export default class UserCover extends React.Component {
    constructor(props) {
        super(props);
    }
    static defaultProps = {
        width: "12",
        pictureAlignment: "center",
        theme: "bg-yellow",
        displayName: "John Doe",
        description: "My profile description",
        displayPicture: "https://scontent.cdninstagram.com/t51.2885-19/10508062_681655528555619_1798483546_a.jpg"
    };

    render() {
        var coverPicture = {}, alignmentType = "widget-user", footerPadding = "";

        if (this.props.pictureAlignment === "left") {
            alignmentType = "widget-user-2";
            footerPadding = "no-padding";
        }

        if (this.props.displayPicture) {
            coverPicture = {
                background: "url(" + this.props.displayPicture + ") center"
            };
        }

        return (
            <div>

                <div className="card kl-card kl-xl kl-reveal kl-fade kl-overlay kl-show kl-slide-in">
                    <div className="kl-card-block kl-md bg-primary kl-shadow-br kl-overlay">
                        <div className="kl-card-overlay kl-card-overlay-split-v kl-dark kl-inverse">
                            <div className="kl-card-overlay-item">
                                <div className="kl-figure-block userFFinfo" >
                                    <span className="kl-figure kl-txt-shadow">{this.props.followed_by}</span>
                                    <span className="kl-title">Followers</span>
                                </div>
                            </div>
                            <div className="kl-card-overlay-item">
                                <div className="kl-figure-block userFFinfo">
                                    <span className="kl-figure">{this.props.follows}</span>
                                    <span className="kl-title">Following</span>
                                </div>
                            </div>
                        </div>
                        <a href="#" className="kl-card-avatar kl-md kl-pm kl-slow"><img className="kl-b-danger kl-b-circle kl-b-3 kl-reveal kl-slow kl-shadow-br kl-spin kl-fade" src={this.props.displayPicture}></img></a>
                        <div className="kl-background">

                        </div>
                        <div className="kl-card-item kl-pbl kl-card-social kl-slide-in">
                            <hr className="hw" />
                            <a href={"http://instagram.com/" + this.props.username} target="_blank"><i className="fa fa-instagram"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

