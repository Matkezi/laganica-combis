import InfoTile from "../../ReusableComponents/LikesNumber/likeTile";
import ProgressBarLike from "../../ReusableComponents/LikesNumber/progressBarLike";
import ProgressBarComment from "../../ReusableComponents/LikesNumber/progressBarComment";
import React from "react";
import { ReactDOM } from "react-dom"


export default class Post extends React.Component {
    constructor(props) {
        super(props);
    }
    static defaultProps = {
        displayName: "John Doe",
        description: "My profile description",
        date: "22.11.1993",
        postPicture: "https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/19931700_335519843553116_809673568055984128_n.jpg",
        width: "12"
    };

    toggleCollapse(event) {
        var box = ReactDOM.findDOMNode(this).children[0],
            boxBody = ReactDOM.findDOMNode(this).children[0].children[1],
            icon = event.currentTarget.children[0];

        commonFunctions.toggleBoxCollapse(box, boxBody, icon);
    }
    removeBox(event) {
        var box = ReactDOM.findDOMNode(this).children[0];
        commonFunctions.removeBox(box);
    }
    render() {
        var postPicture = "", attachments = [], comments = [];

        if (this.props.postPicture) {
            postPicture = <img className="img-responsive pad" src={this.props.postPicture} alt="Photo" />;
        }

        if (this.props.displayStats) {
            return (
                <div className="col-lg-12 box userInstaPost">
                    <div className="box box-widget">
                        <div className="box-header with-border">

                            <div className="user-block">
                                <img src={this.props.displayPicture} className="img-circle" />

                                <span className="description">{this.props.date}</span>
                            </div>

                        </div>
                        <div className="col-lg-6 col-xs-12">
                            {postPicture}

                        </div>

                        <div className="col-lg-6 col-xs-12" id="statTiles">

                            <InfoTile width="12" content='' icon='fa-thumbs-o-up' stats={this.props.likes}
                                subject='Likes' theme='bg-white'>
                                <ProgressBarLike percent={100 * this.props.likes / this.props.followers}
                                    likes={this.props.likes} followers={this.props.followers}
                                    description={100 * this.props.likes / this.props.followers}
                                    color='white' />
                            </InfoTile>

                            <InfoTile width="12" content='' icon='fa-comment-o' stats={this.props.comments}
                                subject='Comments' theme='bg-white'>
                                <ProgressBarComment percent={100 * this.props.comments / this.props.followers}
                                    description={100 * this.props.comments / this.props.followers}
                                    color='white' />
                            </InfoTile>
                        </div>
                        <div className="col-lg-12 col-xs-12 postDescription">
                            <span className="glyphicon glyphicon-heart" aria-hidden="true" />
                            <span className="likesCountDescription">{this.props.likes} likes</span>
                            <p>{this.props.content}</p>
                        </div>
                    </div>
                </div>
            );
        }
        if (!this.props.displayStats) {
            return (
                <div className="col-lg-12 box userInstaPost" >
                    <div className="box box-widget">
                        <div className="box-header with-border">

                            <div className="user-block">
                                <img src={this.props.displayPicture} className="img-circle" />

                                <span className="description">{this.props.date}</span>
                            </div>

                        </div>
                        <div className="col-lg-12">
                            {postPicture}
                        </div>

                        <div className="col-lg-12 postDescription">
                            <span className="glyphicon glyphicon-heart" aria-hidden="true" />
                            <span className="likesCountDescription">{this.props.likes} likes</span>
                            <p>{this.props.content}</p>
                        </div>
                        {/* /.box-footer */}
                    </div>
                </div>
            );
        }
    }
};
