import React from "react";
import { connect } from "react-redux";
import { fetchHome } from "../../../actions/home/home";
import RegisterInstaUserTile from "./RegisterInstaUserTile";
import UserCover from "./userCover";
import { Col, Row, Modal, Button } from "react-bootstrap";
import { isEmpty } from "lodash";
import { fetchUserPostsDetails } from "../../../actions/home/home-user-posts-details";
import ProfileCardRefactor from "./profileCardRefactor";
import Post from "./userPost";
import { SmithxUrls } from "../../../constants/urls";


@connect((store) => {
    return {
        instaUsersInfo: store.home.user_info,
        fetched: store.home.fetched,
        fetchingUsers: store.home.fetching,
        instaDetails: store.home_user_posts_details.user_post_details,
        fetchingPosts: store.home_user_posts_details.fetching,
        fetchedPosts: store.home_user_posts_details.fetched
    };
})
class Home extends React.Component {

    constructor(props) {
        super(props);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.state = { selectedUserIndex: 0, show: false };
        if (localStorage.getItem('home_current__user')) {
            let id = localStorage.getItem('home_current__user');
            this.props.dispatch(fetchHome(id));
        }
        else {
            this.props.dispatch(fetchHome(0));
        }
        this.handleHide = this.handleHide.bind(this);

    }

    handleUserChange(userIndex) {
        localStorage.setItem('home_current__user', JSON.stringify(userIndex));
        this.setState({ selectedUserIndex: userIndex });
    }
    handleHide() {
        this.setState({ show: false });
    }

    render() {

        //Left screen side, list of instagram users
        const mappedUsers = this.props.instaUsersInfo.map((result, index) =>
            <ProfileCardRefactor
                key={index}
                displayName={result.name}
                displayPicture={result.profile_pic}
                following={result.number_follows}
                followers={result.number_followed_by}
                bio={result.bio}
                userIndex={index}
                username={result.username}
                id={result.id}
                onUserChange={this.handleUserChange}

            />);

        // Right screen side, list of user posts
        const mappedPosts = this.props.instaDetails.map((post, index) =>
            <Post
                key={index}
                date={post.created_at}
                postPicture={post.picture_url}
                content={post.caption}
                likes={post.likes}
                comments={post.comments}
                displayPicture={post.displayPicture}
                followers={this.props.instaUsersInfo[this.state.selectedUserIndex].number_followed_by}
                displayStats={true} />
        );


        if (this.props.fetched) {
            if (this.props.instaUsersInfo.length > 0) {
                return (
                    <div className="row">

                        <Col lg={4} md={4} sm={4}>
                            <RegisterInstaUserTile numberOfAccounts={this.props.instaUsersInfo.length} />
                            {mappedUsers}
                        </Col>
                        <Col lg={8} md={8} sm={8}>
                            {this.props.instaUsersInfo[this.state.selectedUserIndex] &&

                                <UserCover
                                    displayPicture={this.props.instaUsersInfo[this.state.selectedUserIndex].profile_pic}
                                    followed_by={this.props.instaUsersInfo[this.state.selectedUserIndex].number_followed_by}
                                    follows={this.props.instaUsersInfo[this.state.selectedUserIndex].number_follows}
                                    username={this.props.instaUsersInfo[this.state.selectedUserIndex].username} />
                            }
                        </Col>
                        <Col lg={8} className="postContainer">
                            {mappedPosts}
                        </Col>

                    </div>
                );
            }
            else {
                return (
                    <div className="card card-cascade wider reverse col-md-4 col-md-offset-4 ">

                        <div className="view overlay hm-white-slight">
                            <img src={"/static/instagram-welcome.jpg"} className="card-img col-xs-12" />
                            <br />

                        </div>
                        <div className="card-body text-center">


                            <h4 className="card-title"><strong>Register Instagram account!</strong></h4>
                            <h5 className="indigo-text"><strong>To make your custom area you have to add Instagram account! </strong></h5>

                            <a onClick={() => this.setState({ show: true })}
                                className="btn btn-automatedLiking">Add an Instagram account</a>
                        </div>

                        <Modal
                            show={this.state.show}
                            onHide={this.handleHide}
                            container={this}
                            aria-labelledby="contained-modal-title"
                        >
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title">
                                    Adding new Instagram account
                    </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                Before adding new Instagram account, please go to <a target="_blank" href="http://www.instagram.com">Instagram</a> and make sure that you are <b>LOGGED OUT</b> from your browser account.
                        <br />

                            </Modal.Body>
                            <Modal.Footer>

                                <a href={SmithxUrls.REACT_APP_REGISTER_INSTAGRAM_ACCOUNT}> <Button className="btn-automatedLiking" onClick={this.handleHide}>Ok, I am logged out in browser, let's do this!</Button> </a>
                            </Modal.Footer>
                        </Modal>

                        <br />

                    </div>
                );
            }
        }
        else {
            return (
                <div></div>
            )
        }
    }
}

export default Home;

