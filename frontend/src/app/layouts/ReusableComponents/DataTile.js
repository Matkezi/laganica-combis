import { Link, Route } from "react-router-dom";
import React from "react"


class DataTile extends React.Component {
    getDefaultProps() {
        return {
            width: "12",
            theme: "bg-yellow",
            icon: "fa fa-instagram",
            subject: "Data",
            stats: "#",
            link: "https://api.instagram.com/oauth/authorize/?client_id=f917af346bb148db93bc4cfef462dc08&redirect_uri=http://localhost:9001/app/insta/login/callback&response_type=code&scope=public_content+comments+likes+follower_list+relationships+basic",
            link_text: "Dataset store"
        };
    }
    render() {

        var link = "",
            stats = <h3> {this.props.number} </h3>;

        if (this.props.link) {
            link =

                <Link to={this.props.link} className="small-box-footer">
                    {this.props.link_text}&nbsp;<i className="fa fa-hdd-o"></i>
                </Link>;
        }

        if (this.props.stats.indexOf("%") !== -1) {
            var style = {
                fontSize: "20px"
            };

            stats =
                <h3>
                    {this.props.stats.replace(/%/g, "")}
                    <sup style={style}>%</sup>
                </h3>;
        }

        return (
            <div className={"col-lg-" + this.props.width + " col-md-" + this.props.width + " col-sm-" + this.props.width + " col-xs-12"}>
                <div className={"small-box " + this.props.theme}>
                    <div className="inner">
                        {stats}
                        <p>{this.props.subject}</p>
                    </div>
                    <div className="icon">
                        <i className={"fa " + this.props.icon}></i>
                    </div>
                    {link}
                </div>
            </div>
        );
    }
};
