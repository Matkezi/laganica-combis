    import React from 'react';
    import ReactDOM from 'react-dom';
    import {WithContext as ReactTags} from 'react-tag-input';
    import Select from 'react-select';

    export default class ChipDropdownInputInput extends React.Component {
        constructor(props) {
            super(props);
            if(this.props.query) {
                this.state = { tags: this.props.query };
            } 
            else {
                this.state = { tags: [] };
            }

            this.handleDelete = this.handleDelete.bind(this);
            this.handleAddition = this.handleAddition.bind(this);
            this.handleDrag = this.handleDrag.bind(this);
            this.handleInputBlur = this.handleInputBlur.bind(this);
               }

        handleDelete(i) {
            let tags = this.state.tags;
            tags.splice(i, 1);
            this.setState({tags: tags});
        }

        handleInputBlur() {
           let tagsToAdd = document.getElementById(this.props.id).value.split(" ");
            let tags = this.state.tags;
            tagsToAdd.forEach(tag => {
                if(tag.length > 0) {
                    tags.push({
                        id: tags.length + 1,
                        text: tag
                    });
                    this.setState({tags: tags});
                }
                
            });
           
        }

        handleAddition(tag) {
            let tags = this.state.tags;
            tags.push({
                id: tags.length + 1,
                text: tag
            });
            this.setState({tags: tags});

        }

        handleDrag(tag, currPos, newPos) {
            let tags = this.state.tags;

            // mutate array
            tags.splice(currPos, 1);
            tags.splice(newPos, 0, tag);

            // re-render
            this.setState({tags: tags});
        }


        state = {    selectedOption: '',      }
        handleChange = (selectedOption) => {
            this.setState({selectedOption});
            let tags = this.state.tags;
            tags.push({
                id: tags.length + 1,
                text: selectedOption.value
            });
            this.setState({tags: tags});
        }

        render() {
            const {tags} = this.state;
            this.props.attributes(tags);
            const {selectedOption} = this.state;
            const value = selectedOption && selectedOption.value;

            return (
                <div style={{margin: '10px 0 0 0'}}>
                    <label>Keywords to search over an area</label>
                    <div className="col-lg-12 col-xs-12 col-md-12">
                    <ReactTags tags={tags}
                             id={this.props.id}
                               classNames={{
                                   tagInput: 'form-group',
                                   tag: 'chips',
                                   remove: 'remove-chip',
                               }}
                               placeholder="What are you searching for?"
                               handleDelete={this.handleDelete}
                               handleAddition={this.handleAddition}
                               handleDrag={this.handleDrag}
                               handleInputBlur={this.handleInputBlur}/>
</div>
<div id={this.props.uniqueId} style={{ display: 'none' }}>
    <h3><b>Please enter at least one keyword over the area you are monitoring.</b></h3>
    </div>

           { /*
                   <Select
                        name="form-field-name"
                        className="form-group"
                        value={value}
                        onChange={this.handleChange}
                        placeholder="Search for objects via image recognition"
                        options={[
                            {value: "person", label: "person"},
                            {value: "bicycle", label: "bicycle"},
                            {value: "car", label: "car"},
                            {value: "motorcycle", label: "motorcycle"},
                            {value: "airplane", label: "airplane"},
                            {value: "bus", label: "bus"},
                            {value: "train", label: "train"},
                            {value: "truck", label: "truck"},
                            {value: "boat", label: "boat"},
                            {value: "traffic light", label: "traffic light"},
                            {value: "fire hydrant", label: "fire hydrant"},
                            {value: "stop sign", label: "stop sign"},
                            {value: "parking meter", label: "parking meter"},
                            {value: "bench", label: "bench"},
                            {value: "bird", label: "bird"},
                            {value: "cat", label: "cat"},
                            {value: "dog", label: "dog"},
                            {value: "horse", label: "horse"},
                            {value: "sheep", label: "sheep"},
                            {value: "cow", label: "cow"},
                            {value: "elephant", label: "elephant"},
                            {value: "bear", label: "bear"},
                            {value: "zebra", label: "zebra"},
                            {value: "giraffe", label: "giraffe"},
                            {value: "backpack", label: "backpack"},
                            {value: "umbrella", label: "umbrella"},
                            {value: "handbag", label: "handbag"},
                            {value: "tie", label: "tie"},
                            {value: "suitcase", label: "suitcase"},
                            {value: "frisbee", label: "frisbee"},
                            {value: "skis", label: "skis"},
                            {value: "snowboard", label: "snowboard"},
                            {value: "sports ball", label: "sports ball"},
                            {value: "kite", label: "kite"},
                            {value: "baseball bat", label: "baseball bat"},
                            {value: "baseball glove", label: "baseball glove"},
                            {value: "skateboard", label: "skateboard"},
                            {value: "surfboard", label: "surfboard"},
                            {value: "tennis racket", label: "tennis racket"},
                            {value: "bottle", label: "bottle"},
                            {value: "wine glass", label: "wine glass"},
                            {value: "cup", label: "cup"},
                            {value: "fork", label: "fork"},
                            {value: "knife", label: "knife"},
                            {value: "spoon", label: "spoon"},
                            {value: "bowl", label: "bowl"},
                            {value: "banana", label: "banana"},
                            {value: "apple", label: "apple"},
                            {value: "sandwich", label: "sandwich"},
                            {value: "orange", label: "orange"},
                            {value: "broccoli", label: "broccoli"},
                            {value: "carrot", label: "carrot"},
                            {value: "hot dog", label: "hot dog"},
                            {value: "pizza", label: "pizza"},
                            {value: "donut", label: "donut"},
                            {value: "cake", label: "cake"},
                            {value: "chair", label: "chair"},
                            {value: "couch", label: "couch"},
                            {value: "potted plant", label: "potted plant"},
                            {value: "bed", label: "bed"},
                            {value: "dining table", label: "dining table"},
                            {value: "toilet", label: "toilet"},
                            {value: "tv", label: "tv"},
                            {value: "laptop", label: "laptop"},
                            {value: "mouse", label: "mouse"},
                            {value: "remote", label: "remote"},
                            {value: "keyboard", label: "keyboard"},
                            {value: "cell phone", label: "cell phone"},
                            {value: "microwave", label: "microwave"},
                            {value: "oven", label: "oven"},
                            {value: "toaster", label: "toaster"},
                            {value: "sink", label: "sink"},
                            {value: "refrigerator", label: "refrigerator"},
                            {value: "book", label: "book"},
                            {value: "clock", label: "clock"},
                            {value: "vase", label: "vase"},
                            {value: "scissors", label: "scissors"},
                            {value: "teddy bear", label: "teddy bear"},
                            {value: "hair drier", label: "hair drier"},
                            {value: "toothbrush", label: "toothbrush"},

                        ]}
                    />
                    */}
                </div>

            )

        }


    }
