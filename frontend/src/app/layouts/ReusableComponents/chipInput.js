import React from 'react';
import ReactDOM from 'react-dom';
import { WithContext as ReactTags } from 'react-tag-input';

export default class ChipInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tags:[],

        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({tags: tags});
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        this.setState({tags: tags});
        console.log(this.state.tags)

    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        // re-render
        this.setState({ tags: tags });
    }

    render() {
        const { tags } = this.state;
         this.props.attributes(tags);
        return (
            <div>
                <ReactTags tags={tags}
                           classNames={{
                               tagInput:'form-group',
                               tag:'chips',
                               remove:'remove-chip',
                           }}
                           placeholder="What are you searching for?
                                              What niche are you engaging?
                                              What would they post? Which tags would they use?
                                              How do they interact between each other?"
                           handleDelete={this.handleDelete}
                           handleAddition={this.handleAddition}
                           handleDrag={this.handleDrag} />
            </div>
        )

    }






}
