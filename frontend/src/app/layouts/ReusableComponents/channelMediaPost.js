import React from "react";
import {CommentForm} from "./commentForm";
import {LikePost} from "./likePost";
import FollowUserForm from "./FollowUser";
var Highlight = require('react-highlighter');
//This front is web instagram picture post, it could be used everywhere, so putting it here
var moment = require('moment');


class ChannelMediaPost extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {

        const mediaComments = this.props.comments.map((result, index) =>
            <span key={index} className="col-lg-12 mediaDesc">
                        <a className="userName" href={"https://www.instagram.com/" + result.user_who_commented } target="_blank">
                                {result.user_who_commented} </a>
                &nbsp;
                {result.comment_text} </span>
        );
        const created_time = moment.unix(this.props.created_time).format("LLLL"); 
        console.log(moment.locale())
        return (
            <article className="mediaBox">
                <div className="row mediaHeader">
                    <div className="col-lg-7 col-md-7 col-sm-7 col-xs-7 mediaInfo">
                        <div className="userNameDiv">
                            <a className="userName" href={"https://www.instagram.com/" + this.props.owner} target="_blank">
                                {this.props.owner}</a>
                        </div>
                        <div >
                            {this.props.media_location}
                        </div>
                    </div>
                </div>
                <div >
                    <a href={this.props.media_url} target="_blank">
                        <img className="leadsMediaPhoto" alt="tags" src={this.props.media_src} />
                    </a>
                    <div className="row mediaFooter">
                        <h4 className="col-lg-12"><p className="dateTimeMedia"><i className="fa fa-clock-o"></i>&nbsp;{created_time}</p></h4>
                        <span className="col-lg-12 mediaDesc">
                        <a className="userName" href={"https://www.instagram.com/" + this.props.owner} target="_blank">
                                {this.props.owner}</a>
                            &nbsp;
                            <Highlight search="Split" caseSensitive={false}> {this.props.media_desc} </Highlight></span>
                        <div className="mediaComments">
                            {mediaComments}
                        </div>
                    </div>
                </div>

            </article>

        );
    }
}
export default ChannelMediaPost;

