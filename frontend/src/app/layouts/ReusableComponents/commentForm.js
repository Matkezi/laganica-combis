import * as React from "react";
import DjangoCSRFToken from "django-react-csrftoken";
import {commentAdd} from "../../actions/shared/comment";
import {connect} from "react-redux";


@connect((store) => {
    return {
        like_comment: store.comment.comment,
        fetched: store.comment.fetched,
        error: store.comment.error

    };
})
export class CommentForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = { value: true, comment: " " };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeComment = this.onChangeComment.bind(this);

    }
    onSubmit(e) {
        e.preventDefault();

        this.props.dispatch(commentAdd(localStorage.getItem("current_instagram_username")),
            this.state.comment,
            this.props.media_id);
            this.setState({ value: '' });
    }

    onChangeComment(e) {
        this.setState({[e.target.name]: e.target.value});

    }

    render() {
        const {comment} = this.state;
        return (
            <div className="col-lg-12 commentForm">
                <form onSubmit={this.onSubmit} className="form-horizontal">
                    <fieldset>
                        <DjangoCSRFToken/>

                        <label className="col-form-label">Comment this with @{localStorage.getItem("current_instagram_username")}:</label>
                        <textarea className="col-lg-10 col-md-10 col-sm-10 col-xs-10" type="text" name="comment"
                                  rows="1"
                                  onChange={this.onChangeComment}
                                  placeholder="Add a comment..." />

                        <button type="submit" className="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-2"
                                id="like-comment-button"
                        disabled={!this.state.value}>
                            <span className="glyphicon glyphicon-send" aria-hidden="true" />
                        </button>
                    </fieldset>
                </form>

            </div>

        );
    }

}
;
