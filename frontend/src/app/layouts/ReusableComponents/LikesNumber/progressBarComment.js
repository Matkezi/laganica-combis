var NumberFormat = require("react-number-format");
import React from "react"

export default class ProgressBarComment extends React.Component {

    static defaultProps = {
        percent: 50,
        description: 'Default progress 50%',
        color: 'white'
    };

    render() {
        var that = this;

        var style = {
            width: this.props.percent + '%',
            backgroundColor: this.props.color
        };

        return (
            <div>
                <div className="progress">
                    <div className="progress-bar" style={style}></div>
                </div>
                <span className="progress-description" id="commScore">
                    <NumberFormat value={this.props.description} displayType={'text'} decimalPrecision={2} />% Comment Score


                        </span>
            </div>
        );
    }
};

