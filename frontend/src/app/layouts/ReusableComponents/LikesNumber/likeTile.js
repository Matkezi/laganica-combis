import React from "react"

export default class InfoTile extends React.Component {

    static defaultProps = {
        content: "",
        icon: "fa-star-o",
        stats: "0",
        subject: "Default Subject",
        theme: "bg-aqua",
        width: "1"
    };
    render() {

        if (this.props.children) {
            return (
                <div >
                    <div className={"info-box " + this.props.theme}>
                        <span className="info-box-icon">
                            <i className={"fa " + this.props.icon}></i>
                        </span>

                        <div className="info-box-content">
                            <span className="info-box-text"><b>{this.props.stats}</b> {this.props.subject}</span>
                            {this.props.children}
                        </div>

                        {this.props.content}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="col-md-3 col-sm-6 col-xs-12">
                    <div className="info-box">
                        <span className={"info-box-icon " + this.props.theme}>
                            <i className={"fa " + this.props.icon}></i>
                        </span>

                        <div className="info-box-content">
                            <span className="info-box-text"><b>{this.props.stats}</b>b> {this.props.subject}</span>
                        </div>

                        {this.props.content}
                    </div>
                </div>
            );
        }
    }
};

