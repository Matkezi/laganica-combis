import React from "react";
import {CommentForm} from "./commentForm";
import {LikePost} from "./likePost";
import FollowUserForm from "./FollowUser";
var Highlight = require('react-highlighter');
//This front is web instagram picture post, it could be used everywhere, so putting it here


class MediaResult extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {

        const mediaComments = this.props.comments.map((result, index) =>
            <span key={index} className="col-lg-12 mediaDesc">
                        <a className="userName" href={"https://www.instagram.com/" + result.user_who_commented } target="_blank">
                                {result.user_who_commented} </a>
                &nbsp;
                {result.comment_text} </span>
        );

        return (
            <article className="mediaBox">
                <div className="row mediaHeader">
                    <div className="col-lg-1 col-xs-1 col-md-1 col-sm-1">
                        <a className="leadsUserLink" href={"https://www.instagram.com/" + this.props.owner} target="_blank">
                            <img className="leadsUserPhoto"
                                 src={this.props.owner_profile_pic}
                                 target="_blank"/>
                        </a>
                    </div>
                    <div className="col-lg-7 col-md-7 col-sm-7 col-xs-7 mediaInfo">
                        <div className="userNameDiv">
                            <a className="userName" href={"https://www.instagram.com/" + this.props.owner} target="_blank">
                                {this.props.owner}</a>
                        </div>
                        <div >
                            {this.props.media_location}
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-3 col-xs-3 col-sm-3">
                        <FollowUserForm user_to_follow={this.props.owner_id} insta_name={this.props.owner}/>
                    </div>
                </div>
                <div >
                    <a href={this.props.media_url} target="_blank">
                        <img className="leadsMediaPhoto" alt="tags" src={this.props.media_src} />
                    </a>
                    <div className="row mediaFooter">
                        <LikePost media_id={this.props.media_id}/>
                        <h4 className="col-lg-12"><b>{this.props.media_likes} likes</b></h4>
                        <span className="col-lg-12 mediaDesc">
                        <a className="userName" href={"https://www.instagram.com/" + this.props.owner} target="_blank">
                                {this.props.owner}</a>
                            &nbsp;
                            <Highlight search="Split" caseSensitive={false}> {this.props.media_desc} </Highlight></span>
                        <div className="mediaComments">
                            {mediaComments}
                        </div>
                        <div className="comment">
                            <CommentForm media_id={this.props.media_id}/>
                        </div>
                    </div>
                </div>

            </article>

        );
    }
}
export default MediaResult;

