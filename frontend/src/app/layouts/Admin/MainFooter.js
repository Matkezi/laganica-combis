import React from "react";


class Footer extends React.Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs"><b>Krv spašava!</b></div>
                <strong>Copyright &copy; 2018 <a href="#">Krv je život</a>. </strong>
            </footer>
        );
    }
}

export default Footer;
