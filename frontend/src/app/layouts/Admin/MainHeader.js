import React from "react";
import ControlSidebarToggle from "../../../adminlte/MainHeader/ControlSidebarToggle";
import Logo from "../../../adminlte/MainHeader/Logo";
import MainSidebarToggle from "../../../adminlte/MainHeader/MainSidebarToggle";
import Nav from "../../../adminlte/MainHeader/Nav";
import Navbar from "../../../adminlte/MainHeader/Navbar";
import Menu from "../../../adminlte/MainHeader/Menu";
import UserMenu from "../../../adminlte/MainHeader/UserMenu";
import Wrapper from "../../../adminlte/MainHeader/Wrapper";
import InstagramUserMenu from "../../../adminlte/MainHeader/InstagramUserMenu";





class TopNavbar extends React.Component {
    render() {
        const {actions, adminlte} = this.props;

        return (
            <Wrapper>
                <Logo/>
                <Navbar>
                    <InstagramUserMenu/>
                    <MainSidebarToggle actions={actions} adminlte={adminlte}/>
                    <Menu>
                        <Nav>
                            <UserMenu actions={actions} adminlte={adminlte}/>
                        </Nav>
                    </Menu>
                </Navbar>
            </Wrapper>
        );
    }
}

export default TopNavbar;
