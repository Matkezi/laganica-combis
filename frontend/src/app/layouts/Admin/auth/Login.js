import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { Link } from "react-router-dom";
import { required } from "redux-form-validators"
import FacebookLogin from 'react-facebook-login';

import { loginUser, fbLogin } from "../../../actions/user/authActions";
import { renderField, renderError } from "../../../utils/renderUtils";
class Login extends Component {

    static propTypes = {
        ...propTypes
    };

    responseFacebook = (response) => {
        this.props.dispatch(fbLogin(response.accessToken))
    }

    render() {
        const { handleSubmit, error } = this.props;

        return (

            <div className="hold-transition login-page" id="loginScreen">


                <div className="login-box">
                    <div className="login-logo">
                        <a href="#"><b>Krv je život</b></a>
                        <p id="smithx-crumbs">Nije bogat onaj koji puno ima, nego onaj kome malo treba.</p>
                    </div>

                    <div className="login-box-body">
                    <div className="col-lg-12 fb-row">
                            <FacebookLogin
                                appId="1814301338582458"
                                autoLoad={false}
                                fields="name,email,picture"
                                callback={this.responseFacebook}
                                cssClass="col-lg-12 col-xs-12 loginBtn loginBtn--facebook"
                                textButton="Login Facebookom" />
                        </div>
                        
                        <br />
                        <hr />
                        <div className="row justify-content-center">
                        <hr />
                            <form
                                className="col-xs-12"
                                onSubmit={handleSubmit}
                            >


                                <fieldset className="form-group login-form" >
                                    <Field name="email" label="Email" component={renderField}
                                        type="text" validate={[required({ message: "This field is required." })]}
                                    />
                                </fieldset>


                                <fieldset className="form-group login-form">
                                    <Field name="password" label="Lozinka" component={renderField}
                                        type="password" validate={[required({ message: "This field is required." })]}
                                    />
                                </fieldset>
                                <br />
                                <fieldset className="form-group">
                                    {renderError(error)}
                                    <button action="submit" className="btn btn-primary col-lg-12 col-xs-12">Login emailom</button>
                                </fieldset>
                                <hr />

                                <p className="login-form"><b>Niste registrirani?</b> <Link className="login-form" to="/app/signup">
                                    <button className="btn btn-primary ">Registracija emailom</button>
                                </Link></p>
                                <Link className="login-form" to="/app/reset_password">Ooups, zaboravili lozinku?</Link>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default reduxForm({
    form: "login",
    onSubmit: loginUser
})(Login);
