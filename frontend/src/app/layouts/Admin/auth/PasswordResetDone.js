import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class PasswordResetDone extends Component {
    render() {
        return (
              <div className="hold-transition login-page" id="loginScreen">
                <div className="login-box">

                    <div className="login-box-body">

                        <div className="row justify-content-center">

            <h3 className="mx-5     login-form">An  password reset email has been sent to your email. Please follow the link to reset your password.</h3>
                     <Link className="login-form" to="/login">
                                    <button  className="btn-sm btn-primary col-xs-12">Back to log in page!</button>
                                </Link>
                        </div>
                    </div>
                </div>
              </div>
        )
    }
}