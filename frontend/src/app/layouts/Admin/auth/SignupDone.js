import React, { Component } from "react";

export default class SignupDone extends Component {
    render() {
        return (


            <div className="hold-transition login-page" id="loginScreen">
                    <div className="login-logo">
                        <a href="#"><b>Smith</b>X</a>
                    </div>
            <h3 className="login-form col-xs-12">
                Thanks for your registration, please follow the link sent to your provided email to activate
                your account. If you have not received an email, please check your SPAM folder.
            </h3>
                        </div>

        )
    }
}