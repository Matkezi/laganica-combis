import React, { Component } from "react";
import { reduxForm, Field, propTypes } from "redux-form";
import { required } from "redux-form-validators"

import { renderField, renderError} from "../../../utils/renderUtils";
import { resetPassword } from "../../../actions/user/authActions";

class PasswordReset extends Component {

    static propTypes = {
        ...propTypes
    };

    render() {
        const { handleSubmit, error } = this.props;

        return (
            <div className="hold-transition login-page" id="loginScreen">
                <div className="login-box">
                    <div className="login-logo">
                        <a href="#"><b>Smith</b>X</a>
                        <p id="smithx-crumbs">Social Management Instagram Tool</p>
                    </div>
                    <div className="login-box-body">

                        <div className="row justify-content-center">

                <form
                    className="col-xs   -12 login-form"
                    onSubmit={handleSubmit}
                >
                    <h4 className="text-md-center">Reset Your Password</h4>
                    <hr/>

                    <fieldset className="form-group">
                        <Field name="email" label="Please enter your email" component={renderField}
                               type="text" validate={[required({message: "This field is required."})]}
                        />
                    </fieldset>

                    <fieldset className="form-group">
                        { renderError(error) }
                        <button action="submit" className="btn btn-primary col-xs-12">Submit</button>
                    </fieldset>
                </form>
            </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default reduxForm({
    form: "password_reset",
    onSubmit: resetPassword
})(PasswordReset);
