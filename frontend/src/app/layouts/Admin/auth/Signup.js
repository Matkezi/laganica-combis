import React, { Component } from "react";
// import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { required } from "redux-form-validators"
import { renderField, renderError } from "../../../utils/renderUtils";
import { signupUser } from "../../../actions/user/authActions";

class Signup extends Component {

    static propTypes = {
        ...propTypes
    };


    render() {
        const { handleSubmit, error } = this.props;

        return (
              <div className="hold-transition login-page" id="loginScreen">
                <div className="login-box">
                    <div className="login-logo">
                        <a href="#"><b>Krv je život</b></a>
                        <p id="smithx-crumbs">Nije bogat onaj koji puno ima, nego onaj kome malo treba.</p>
                    </div>
                    <div className="login-box-body">

                        <div className="row justify-content-center">

                <form
                    className="col-xs-12 login-form"
                    onSubmit={handleSubmit}
                >
                    <h4 className="text-md-center">Postani donator</h4>
                    <hr/>

                    <fieldset className="form-group">
                        <Field  name="email" label="Email" component={renderField}
                               type="text"/>
                    </fieldset>

                    <fieldset className="form-group">
                        <Field name="username" label="Username" component={renderField}
                               type="text" validate={[required({message: "This field is required."})]}
                        />
                    </fieldset>

                    <fieldset className="form-group">
                        <Field name="password1" label="Password" component={renderField}
                               type="password" validate={[required({message: "This field is required."})]}
                        />
                    </fieldset>

                    <fieldset className="form-group">
                        <Field name="password2" label="Confirm Password" component={renderField}
                               type="password" validate={[required({message: "This field is required."})]}
                        />
                    </fieldset>

                    { renderError(error) }
                    <p id="registerPolicy">Krv je život u surađuje sa Hrvatskim zavodom za transfuzijsku medicinu (HZTM) kako bi vodio registar o vašim dobrovoljnim darivanjima, te ukupnim zalihama krvi za darivanje.
<a style={{color: 'white'}} href="http://smithx.io/privacy" target="_blank">Privacy Policy</a>.</p>
<input type="checkbox" /> <label> Prihvaćam <a href="/">Uvjete korištenja</a></label>
<br/>

                    <fieldset className="form-group">
                        <button action="submit" className="btn btn-primary col-xs-12">Sign Up</button>
                    </fieldset>
                </form>
            </div>
                    </div>
                </div>
              </div>
        );
    }
}

// Sync field level validation for password match
const validateForm = values => {
    const errors = {};
    const { password1, password2 } = values;
    if (password1 !== password2) {
        errors.password2 = "Password does not match."
    }

    return errors;
};

export default reduxForm({
    form: "signup",
    validate: validateForm,
    onSubmit: signupUser
})(Signup);
