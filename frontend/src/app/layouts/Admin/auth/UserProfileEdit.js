import React, { Component } from "react";
import { reduxForm, Field, propTypes, FieldArray } from "redux-form";
import { connect } from 'react-redux'
import { required } from "redux-form-validators"
import { renderField, renderTextAreaField, renderError, renderAvailability, RenderAvailability, renderToggle } from "../../../utils/renderUtils";



import { updateUserProfile } from "../../../actions/user/authActions";

class UserProfileEdit extends Component {

    constructor(props) {
        super(props);
    }

    static propTypes = {
        ...propTypes
    };

    render() {
        const { handleSubmit, error, initialValues } = this.props;


        return (
            <div className="row justify-content-center ">
                <form
                    className="container user-desc"
                    onSubmit={handleSubmit}
                >
                    <h4 className="text-md-center">Vaš Profil</h4>
                    <hr />

                    <fieldset className="form-group">
                        <label>Slati email tvrtci?</label>
                        <Field name="send_company_email" component={renderToggle}
                            type="checkbox"
                        />
                    </fieldset>


                    <fieldset className="form-group">
                        <Field disabled={true} name="first_name" label="Ime" component={renderField}
                            type="text"
                        />
                    </fieldset>

                    <fieldset className="form-group">
                        <Field disabled={true} name="last_name" label="Prezime" component={renderField}
                            type="text"
                        />
                    </fieldset>

                    <fieldset className="form-group">
                        <Field name="company_email" label="Email" component={renderField}
                            type="email"
                        />
                    </fieldset>


                    <fieldset className="form-group">
                        <Field name="telephone" label="Broj telefona" component={renderField}
                            type="text"
                        />
                    </fieldset>
                    <br /><br />
                    <fieldset className="form-group">
                        {renderError(error)}
                        <button action="submit" className="btn btn-primary col-sm-12">Spremi</button>
                    </fieldset>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        initialValues: state.auth.user
    }
}

export default connect(mapStateToProps)(reduxForm({
    form: "update_user_profile",
    onSubmit: updateUserProfile
})(UserProfileEdit));
