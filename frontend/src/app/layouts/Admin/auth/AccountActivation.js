import React, { Component } from "react";
import { reduxForm, propTypes } from "redux-form";
import { activateUserAccount } from "../../../actions/user/authActions";
import { renderError } from "../../../utils/renderUtils";

class AccountActivation extends Component {

    static propTypes = {
        ...propTypes
    };

    render() {
        const { handleSubmit, error } = this.props;

        return (

            <div className="hold-transition login-page" id="loginScreen">
                <div className="login-box">
                    <div className="login-logo">
                        <a href="#"><b>Smith</b>X</a>
                        <p id="smithx-crumbs">Social Management Instagram Tool</p>
                    </div>
                    <div className="login-box-body">

                        <div className="row justify-content-center">

                            <form
                                className="col-xs-12 login-form"
                                onSubmit={handleSubmit}
                            >
                                <h4 className="text-md-center">Please click the button below to activate your account</h4>
                                <hr/>

                                <fieldset className="form-group">
                                    {renderError(error)}
                                    <button action="submit" className="btn btn-primary col-xs-12">Activate</button>
                                </fieldset>
                            </form>
                        </div>
                    </div></div></div>
        );
    }
}

export default reduxForm({
    form: "user_account_activation",
    onSubmit: activateUserAccount,
})(AccountActivation);
