import * as React from "react";
import { connect } from "react-redux";
import { register_instagram_account } from "../../../actions/shared/registerInstagramAccount";
import { Redirect } from 'react-router-dom'


@connect((store) => {
    return {
        register: store.registerInstagramAccount.register,
        fetched: store.registerInstagramAccount.fetched,
        fetching: store.registerInstagramAccount.fetching,
    };
})
class RegisterInstagramAccount extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        var code = this.props.location.search.split("=")[1];
        console.log(code);
        this.props.dispatch(register_instagram_account(code));
    }

    render() {
        if (this.props.fetched) {
            return (
                <Redirect to='/app/home' />
            );
        }
        else {
            return (
                <div>
                    Saving Instagram account to your profile..
                </div>
            );
        }



    }

}
export default RegisterInstagramAccount;

