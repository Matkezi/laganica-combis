import React from "react";
import Menu from "../../../adminlte/MainSidebar/Menu";
import Wrapper from "../../../adminlte/MainSidebar/Wrapper";



class LeftSidebar extends React.Component {
    render() {
        return (
            <Wrapper>
                <Menu />
            </Wrapper>
        );
    }
}

export default LeftSidebar;
