import React, {Component} from "react";
import { Notifs } from 'redux-notifications';
import PropTypes from "prop-types";

import MainContent from "./MainContent";


export default class App extends Component {

    render() {
            return (
                <div>
                    <Notifs/>
                    <MainContent/>
                </div>
            );
        }


}