import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import RequireAuth from "../Admin/auth/RequireAuth";
import Login from "../Admin/auth/Login";
import Logout from "../Admin/auth/Logout";
import Signup from "../Admin/auth/Signup";
import SignupDone from "../Admin/auth/SignupDone";
import AccountActivation from "../Admin/auth/AccountActivation";
import UserProfileEdit from "../Admin/auth/UserProfileEdit";
import PasswordChange from "../Admin/auth/PasswordChange";
import PasswordReset from "../Admin/auth/PasswordReset";
import PasswordResetDone from "../Admin/auth/PasswordResetDone";
import PasswordResetConfirm from "../Admin/auth/PasswordResetConfirm";
import Home from "../Home/LandingPage";
import Admin from "../Admin";
import Donors from "../Administrator/Donors/index.js";
import ClientProfile from "../Client/Profile/index.js";
import CreateDonorForm from "../Administrator/Donors/createDonorForm.js"
import NoMatch from "./NoMatch";
import { BrowserRouter } from 'react-router-dom'
import Stock from "../Administrator/Stock/index"
import Landing from "../Client/Landing/index"
import Donation from "../Client/DonationDetails/index"
const MainContent = () => (
    <div>
        <Switch>
            <Route path="/app/login" component={Login} />
            <Route path="/app/logout" component={Logout} />
            <Route path="/app/signup" component={Signup} />
            <Route path="/app/account/confirm-email/:key" component={AccountActivation} />
            <Route path="/app/signup_done" component={SignupDone} />
            <Route path="/app/reset_password" component={PasswordReset} />
            <Route path="/app/reset_password_done" component={PasswordResetDone} />
            <Route path="/app/reset/:uid/:token/" component={PasswordResetConfirm} />


            <Admin>
                <Switch>
                <Route component={RequireAuth(ClientProfile)} path={"/app/profile"}/>
                <Route component={RequireAuth(Donation)} path={"/app/donation/:id"}/>
                    <Route component={Stock} path={"/app/stock"} />

                    <Route component={CreateDonorForm} path={"/app/create-donor"} />

                    <Route component={Donors} path={"/app/donors"} />
                    <Route component={ClientProfile} path={"/app/profile"} />

                    <Route component={RequireAuth(Landing)} path="/app/" />
                    <Route component={RequireAuth(UserProfileEdit)} path={"/app/profile_edit"} />
                    <Route component={RequireAuth(PasswordChange)} path={"/app/change_password"} />
                    <Route path="*" component={RequireAuth(Landing)} />

                </Switch>
            </Admin>

        </Switch>


    </div>
);

export default MainContent;