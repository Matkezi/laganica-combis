import React from "react";
import { connect } from "react-redux";
import { Row, Alert } from 'react-bootstrap';
var QRCode = require('qrcode.react');


class DonationDetails extends React.Component {


    render() {
        return (

            <div>
                <QRCode value={"http://localhost:9000/smithx/donation-confirmation/"+this.props.id}
                    size="350" />
                <br />
                <br />

                <Alert bsStyle="warning">
                    <strong>Molimo pokažite ovo Vašoj medicinskoj sestri!</strong>
                    <br />
                    Vaša tvrtka će dobiti potvrdu da ste donirali krv.
   <br /> Ako ne želite da im automatski pošaljemo potvrdu, molimo isključite tu opciju u postavkama Vašeg profila.
</Alert>


            </div>

        );
    }
}
export default DonationDetails;

