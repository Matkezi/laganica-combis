import React from "react";
import { connect } from "react-redux";
import { fetchDonation } from "../../../actions/Client/Donation/fetchDonation.js";
import DonationDetails from "./Details.js"
import { Tooltip, OverlayTrigger, Panel, Breadcrumb, PageHeader } from 'react-bootstrap';


@connect((store) => {
    return {
        donation: store.fetchDonation.donation,
        fetched_donation: store.fetchDonation.fetched,
        fetching_donation: store.fetchDonation.fetching,

    };
})
class Donation extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        const username = localStorage.getItem('current_instagram_username');
        const {id} = this.props.match.params.id

        return (

            <div className="container" >
                <DonationDetails
                    id={this.props.match.params.id}
                />

                <h3>
                    <i> Upute za prehranu prije darivanja </i>
                </h3>
                <hr />

                <img src="https://i.imgur.com/YrokCVN.jpg" className="prehrana" alt="Upute za prehranu" />
                <img src="https://i.imgur.com/z19AfSy.jpg" className="prehrana" alt="Upute za prehranu" />
                <img src="https://i.imgur.com/qJthvWt.jpg" className="prehrana" alt="Upute za prehranu" />


            </div>
        );


    }
}
export default Donation;

