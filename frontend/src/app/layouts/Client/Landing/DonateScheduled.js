import React from "react";
import { connect } from "react-redux";
import { Row, Alert, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";

class DonateScheduled extends React.Component {
    constructor(props, context) {
        super(props, context);
    
        this.handleDismiss = this.handleDismiss.bind(this);
        this.handleShow = this.handleShow.bind(this);
    
        this.state = {
          show: true
        };
      }

      
  handleDismiss() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  
    render() {

    if (this.state.show) {
      return (
        <Alert bsStyle="danger" onDismiss={this.handleDismiss} >
          <h4>Vrijeme je za tvoju donaciju broj {this.props.donation_number}!</h4>
          <p>
            Hvala, što ćete doći na darivanje! Imamo nestašicu Vaše krvne grupe i puno ćete nam pomoći!
            <br/>
            Vidimo se u bolnici <b/>{this.props.location}<b/>, dana <b/>{this.props.donation_date}<b/>. 
          </p>
          <p>
           <Link to={"/app/donation/" + this.props.id}> <Button bsStyle="danger">Detalji Vašeg darivanja</Button></Link>
           
          </p>
        </Alert>
      );
    }

    return <Button bsStyle="danger" onClick={this.handleShow}>Vaše sljedeće darivanje</Button>;
  }

    
}
export default DonateScheduled;

