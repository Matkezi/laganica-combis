import React from "react";
import { connect } from "react-redux";
import { fetchDonations } from "../../../actions/Client/Landing/fetchDonations.js";
import { fetchPending } from "../../../actions/Client/pendingDonation.js";
import { donateResponse } from "../../../actions/Client/donateResponse.js";
import  Donations  from "./Donations.js"
import  DonateScheduled  from "./DonateScheduled.js"
import { Tooltip, OverlayTrigger, Panel, Breadcrumb,Row, Modal, Button, ProgressBar, Jumbotron, Alert } from 'react-bootstrap';
import DonationPending from "../DonationPending.js"

@connect((store) => {
    return {
        donation_list: store.fetchDonations.donation_list,
        fetched_donations: store.fetchDonations.fetched,
        fetching_donations: store.fetchDonations.fetching,
        pendingStatus: store.pendingDonation.pending

    };
})
class Landing extends React.Component {

    constructor(props) {
        super(props);
        this.props.dispatch(fetchDonations(localStorage.getItem("current_instagram_username")));
        this.props.dispatch(fetchPending(localStorage.getItem("current_instagram_username")));

      }

    


    render() {
        if(!this.props.pendingStatus.pending)
        {
        if(this.props.fetched_donations) {
       
       const username = localStorage.getItem('current_instagram_username');

        const donations = this.props.donation_list.data.map((donation, index) => {
            if (donation.donation_made) {
                return ( 
                    <div className="col-lg-3 donation-wrapper">
                <Donations 
                key={donation.id}
                donation_number={this.props.donation_list.count - index}
                donation_date={donation.donation_date}
                location={donation.location}
              />
              </div>

              );
            }
        });
           
              
        const donateScheduled = this.props.donation_list.data.map((donation, index) => {
            if (!donation.donation_made) {
                return ( <div key={donation.id} style={{margin: '15px'}}>
                    <ul class="list-group">
                <DonateScheduled 
                id={donation.id}
                donation_number={donation.donation_number}
                donation_date={donation.donation_date}
                location={donation.location}
              />
              </ul>
             </div>);
            }
        }
          
        
        );

        return (

            <div className="container" >
            <br/>
            <div className="col-lg-6">
            <Jumbotron>
  <h1 style={{fontSize:'42px'}}>Pozdrav, Matko!</h1>
  <p>
    Čestitamo na Vašoj jubilarnoj desetoj donaciji! Sigurni smo da tu ne stajete, pa Vam postavljamo novi cilj na 15 darivanja krvi. Sretno!
  </p>
  <p>
  <ProgressBar now={66} label="Još 5 darivanja do cilja" />
  </p>
</Jumbotron>
<br/>
<br/>

</div>
<br/>
           
            {donateScheduled}
            <div className="col-lg-12">
                {donations}
                </div>

                
            </div>
        );
    }
    else {
        return ( <div> <img id="loading-icon"src="http://www.myiconfinder.com/uploads/iconsets/256-256-2f924348a1185b4446235ba6e1977147.png" /> </div>);
    }
    }
    else {
        return (
            <DonationPending />);
    }
}
}
export default Landing;

