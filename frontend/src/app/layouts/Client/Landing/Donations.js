import React from "react";
import { connect } from "react-redux";
import { Row } from 'react-bootstrap';


class Donations extends React.Component {


    render() {
        if(this.props.donation_number == 5 || this.props.donation_number == 10 || this.props.donation_number == 15)
        {
            return (

                <div className="donation-card donation-transition gold">
                    <h2 className="donation-h2 donation-transition">{this.props.donation_number}*</h2>
                    <p className="donation-p donation-transition">{this.props.location} <br/> <br/>  <b>{this.props.donation_date}</b> </p>
                    <div className="donation-card_circle donation-transition"></div>
                </div>
    
    
            );
        }
        else {
            return (

                <div className="donation-card donation-transition">
                    <h2 className="donation-h2 donation-transition">{this.props.donation_number}</h2>
                    <p className="donation-p donation-transition">{this.props.location} <br/> <br/>  <b>{this.props.donation_date}</b> </p>
                    <div className="donation-card_circle donation-transition"></div>
                </div>
    
    
            );
        }

    }
}
export default Donations;

