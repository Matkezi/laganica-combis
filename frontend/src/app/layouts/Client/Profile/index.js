import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getUserProfile } from "../../../actions/user/authActions";
import UserProfile from "./UserProfile";
import { Row } from "react-bootstrap";
import { fetchPending } from "../../../actions/Client/pendingDonation.js";
import DonationPending from "../DonationPending.js"

@connect((store) => {
    return {
        pendingStatus: store.pendingDonation.pending

    };
})
class ClientProfile extends Component {

    static propTypes = {
        getUserProfile: PropTypes.func.isRequired,
        user: PropTypes.object
    };

    componentWillMount() {
        this.props.getUserProfile();
        this.props.dispatch(fetchPending(localStorage.getItem("current_instagram_username")));

    }

    render() {
        if(!this.props.pendingStatus.pending)
        {
            return (
                <Row>
                         <UserProfile />
                </Row>
             );
        }
        else if (this.props.pendingStatus) {
            return (
                <DonationPending />);
        }

    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

export default connect(mapStateToProps, { getUserProfile } )(ClientProfile);