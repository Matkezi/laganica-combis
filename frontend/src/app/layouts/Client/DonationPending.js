import React from "react";
import { connect } from "react-redux";

import { donateResponse } from "../../actions/Client/donateResponse.js";

import { Tooltip, OverlayTrigger, Panel, Breadcrumb,Row, Modal, Button } from 'react-bootstrap';


@connect((store) => {
    return {
        pendingStatus: store.pendingDonation.pending

    };
})
class DonationPending extends React.Component {

    constructor(props) {
        super(props);
   
      }

  
    render() {
    
  
        return (
        <div className="static-modal">
  <Modal.Dialog>
    <Modal.Header>
      <Modal.Title>Pozvani ste na doniranje krvi!</Modal.Title>
    </Modal.Header>

    <Modal.Body>Hitno nam nedostaje Vaša krva grupa. Jeste li u mogućnosti doći u najbližu bolnicu?</Modal.Body>

    <Modal.Footer>
      <Button bsStyle="success" onClick={() => { this.props.dispatch(donateResponse(localStorage.getItem("current_instagram_username"), "Yes"));}}>Jesam</Button>
      <Button bsStyle="danger" onClick={() => { this.props.dispatch(donateResponse(localStorage.getItem("current_instagram_username"), "No"));}}>Nisam</Button>

    </Modal.Footer>
  </Modal.Dialog>
</div> );
    }
}

export default DonationPending;

