import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import rootReducer from './rootReducer';
import {loadingBarMiddleware} from 'react-redux-loading-bar';

function configureStore(initialState) {
    let createStoreWithMiddleware;

    const logger = createLogger();
    const middleware = applyMiddleware(thunk, loadingBarMiddleware(), logger);

    createStoreWithMiddleware = compose(
        middleware
    );

    return createStoreWithMiddleware(createStore)(rootReducer, initialState);
}

let store = configureStore();

export default store;