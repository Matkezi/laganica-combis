import React from "react";
import { reduxForm, Field, propTypes, FieldArray } from "redux-form";
import Switch from 'react-bootstrap-switch';

export const renderField = ({ input, label, placeholder, disabled, type, meta: { touched, error } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input className="form-control" {...input} type={type} placeholder={placeholder} disabled={disabled} />
        </div>
        {touched && ((error && <div className="alert alert-danger p-1"><small>{error}</small></div>))}
    </div>
);


export const renderToggle = ({ input, type, meta: { touched, error } }) => (
<div>
<label class="switch">
  <input {...input} type={type}  />
  <span class="slider round"></span>
  {touched && ((error && <div className="alert alert-danger p-1"><small>{error}</small></div>))}
</label>
</div>
);

export const SearchEmployee = ({input, type, meta: { touched, error }}) => (
    <Switch  bsSize="large" onText="Traži posao" offText="Traži zaposlenike" onColor="info" offColor="info"/>
);

export const renderTextAreaField = ({ input, label, rows, placeholder, type, meta: { touched, error } }) => (
    <div>
        <label>{label}</label>
        <div>
            <textarea rows={rows} placeholder={placeholder} className="form-control" {...input} type={type} />
        </div>
        {touched && ((error && <div className="alert alert-danger p-1"><small>{error}</small></div>))}
    </div>
);

export const renderError = (errorMessages) => {
    if (errorMessages) {
        return (
            <div className="alert alert-danger">
                {errorMessages}
            </div>
        )
    }
};
