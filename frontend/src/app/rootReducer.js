import { combineReducers } from "redux";
import adminlte from "../adminlte/reducers";
import alerts from "./reducers/alerts";
import home from "./reducers/home/home";
import { reducer as formReducer } from "redux-form";
import authReducer from "./reducers/authReducer"
import { reducer as notifReducer } from 'redux-notifications';
import fetchDonors from "./reducers/Administrator/fetchDonors"
import createDonor from "./reducers/Administrator/createDonor"
import fetchDonations from "./reducers/Client/Landing/fetchDonations"
import fetchDonation from "./reducers/Client/Donation/fetchDonation"

import {loadingBarReducer}  from 'react-redux-loading-bar'
import fetchStock from "./reducers/Administrator/fetchStock"
import updateStock from "./reducers/Administrator/updateStock"
import pendingDonation from "./reducers/Client/pendingDonation"

export default combineReducers({
    loadingBar: loadingBarReducer,
    notifs: notifReducer,
    adminlte,
    alerts,
    home,
    form: formReducer,
    auth:authReducer,
    fetchDonations,
    fetchDonation,
    fetchDonors,
    createDonor,
    fetchStock,
    updateStock,
    pendingDonation
});
