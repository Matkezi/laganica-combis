import React from "react";
import _ from "lodash";
import { Link, Route } from "react-router-dom";
import Home from "../../app/layouts/Home/LandingPage/index";
import { Tooltip, OverlayTrigger, Panel, Breadcrumb } from 'react-bootstrap';


const allLinks = [
    {
        text: "Home",
        to: "/home",
        icon: "fa fa-home"
    },
    {
        text: "Profile details",
        to: "/home/profile-details",
        icon: "fa fa-users"
    },
  

];

const userLinks = _.filter(allLinks, (link) => {
    return link;
});


export default class Menu extends React.Component {

    constructor() {
        super();
        this.state = {
            home: "",
            dataset: "",
            influencers: "",
            leads: "",
            channels: "",
        };

        if (!localStorage.getItem("current_instagram_username")) {
            this.state = { disableLinks: true };
        }
        else {
            this.state = { disableLinks: false };
        }
    }
    changeHomeToActive = (e) => {
        if (document.getElementsByClassName("sidebar-open")[0]) {
            document.getElementsByClassName("sidebar-toggle")[0].click();
        }

        this.setState({ home: "active" });
        this.setState({ automatedLiking: "" })
        this.setState({ dataset: "" });
        this.setState({ influencers: "" });
        this.setState({ leads: "" });
        this.setState({ channels: "" });
    };

    automatedLikingToActive = (e) => {
        if (document.getElementsByClassName("sidebar-open")[0]) {
            document.getElementsByClassName("sidebar-toggle")[0].click();
        }
        this.setState({ home: "" });
        this.setState({ automatedLiking: "active" })
        this.setState({ dataset: "" });
        this.setState({ influencers: "" });
        this.setState({ leads: "" });
        this.setState({ channels: "" });
    };

    changeDatasetToActive = (e) => {
        if (document.getElementsByClassName("sidebar-open")[0]) {
            document.getElementsByClassName("sidebar-toggle")[0].click();
        }
        this.setState({ dataset: "active" });
        this.setState({ automatedLiking: "" })
        this.setState({ home: "" });
        this.setState({ influencers: "" });
        this.setState({ leads: "" });
        this.setState({ channels: "" });

    };
    changeLeadsToActive = (e) => {
        if (document.getElementsByClassName("sidebar-open")[0]) {
            document.getElementsByClassName("sidebar-toggle")[0].click();
        }
        this.setState({ leads: "active" });
        this.setState({ automatedLiking: "" })
        this.setState({ home: "" });
        this.setState({ dataset: "" });
        this.setState({ influencers: "" });
        this.setState({ channels: "" });

    };
    changeInfluencersToActive = (e) => {
        this.setState({ influencers: "active" });
        this.setState({ automatedLiking: "" })
        this.setState({ home: "" });
        this.setState({ dataset: "" });
        this.setState({ leads: "" });
        this.setState({ channels: "" });

    };

    changeChannelsToActive = (e) => {
        if (document.getElementsByClassName("sidebar-open")[0]) {
            document.getElementsByClassName("sidebar-toggle")[0].click();
        }
        this.setState({ influencers: "" });
        this.setState({ home: "" });
        this.setState({ automatedLiking: "" })
        this.setState({ dataset: "" });
        this.setState({ leads: "" });
        this.setState({ channels: "active" });

    };
    render() {
        return (
            <section className="sidebar">

                <ul className="sidebar-menu tree">
                {localStorage.getItem("current_instagram_profile_picture") &&
                    <li style={{ padding: '6px' }}>
                        <img className="img-circle profile-round-picture" src={localStorage.getItem("current_instagram_profile_picture")} />
                    </li>
                }
                    <li className="header text-center" >Upravljačka ploča</li>

                    <li className={this.state.channels} onClick={this.changeChannelsToActive}>
                        <Link to="/app/">
                            <i className="fa fa-home" />
                            <span>Naslovna</span>
                        </Link>
                    </li>

                    <li className={this.state.dataset} onClick={this.changeDatasetToActive}>

                        <Link to="/app/profile">
                            <i className="fa fa-user" />
                            <span>Profil</span>
                        </Link>
                    </li>

                 



                </ul>
            </section>
        );
    }

}
