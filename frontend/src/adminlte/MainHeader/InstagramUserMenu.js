import React from "react";
import {Button, ButtonGroup, DropdownButton, MenuItem} from "react-bootstrap";
import {connect} from "react-redux";
import {fetchInstagramUserDropdown} from "../../app/actions/shared/InstagramUserDropdown";
import LocalStorageMixin from "react-localstorage";
import history from "../../app/utils/historyUtils";

@connect((store) => {
    return {
        instagramUserDropdown: []
    };  
})
class InstagramUserMenu extends React.Component {
    constructor(props) {
        super(props);
        this.props.dispatch(fetchInstagramUserDropdown());


        if (localStorage.getItem("current_instagram_username")) {
            let username = localStorage.getItem("current_instagram_username");
            this.state = {username: username};
        }

        this.setCurrentAccount = this.setCurrentAccount.bind(this);
    }

    setCurrentAccount(evt, evtKey) {
        this.setState({username: evt.username, profile_picture: evt.profile_pic});
        localStorage.setItem("current_instagram_username", evt.username);
        localStorage.setItem("current_instagram_profile_picture", evt.profile_pic);
        localStorage.setItem("current_instagram_id", evt.user_id);
        history.push(window.location.pathname);
    }

  

    render() {


        const mappedInstagramUsers = this.props.instagramUserDropdown.map((result) =>
            <MenuItem eventKey={result} key={result.username}>
                <img className="img-circle" id="leadsOwnerPic" src={result.profile_pic} alt={result.username}/>
                <span className="dropdown-username">@{result.username}</span>
            </MenuItem>);
            // if any instagram acc exists and local storage is empty, select first account in list to be selected in dropdown
        if (this.props.instagramUserDropdown.length > 0) {
               if (!localStorage.getItem("current_instagram_username")) {
            this.state = {username: this.props.instagramUserDropdown[0].username};
            let username = localStorage.setItem("current_instagram_username", this.state.username);
            localStorage.setItem("current_instagram_profile_picture", this.props.instagramUserDropdown[0].profile_pic );
            localStorage.setItem("current_instagram_id", this.props.instagramUserDropdown[0].user_id);
            window.location.reload();

        }
            return (

                <ButtonGroup id="instagramUserDropdown">
                    <DropdownButton title={"@ " + this.state.username} id="bg-nested-dropdown"
                                    onSelect={this.setCurrentAccount}>
                        {mappedInstagramUsers}
                    </DropdownButton>
                </ButtonGroup>

            );
        }
        else if (this.props.instagramUserDropdown.length == 0){
            return (
                <div></div>
            );
        }

    }
}

export default InstagramUserMenu;
