import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import classNames from "classnames";
import {getUserProfile} from "../../app/actions/user/authActions";


class UserMenu extends React.Component {
       static propTypes = {
        getUserProfile: PropTypes.func.isRequired,
        user: PropTypes.object
    };

    componentWillMount() {
        this.props.getUserProfile();
    }

    render() {
         const user = this.props.user;

        const {actions, adminlte} = this.props;
        const userPanel = adminlte.getIn(["mainHeader", "userPanel"]);

        const classes = classNames({
            "dropdown": true,
            "user": true,
            "user-menu": true,
            "open": ! userPanel.get("collapsed")
        });

        return (
            <li className={classes}>
                <a
                    onClick={actions.mainHeaderUserMenuToggle}
                    className="cursor-pointer dropdown-toggle"
                >

                    <i className="fa fa-user" id="user-icon" />
                </a>

                <ul className="dropdown-menu">
                    <li className="user-header">



                    </li>

                    <li className="user-body">
                        <div className="col-md-12">
                            Krv je život! Hvala što donirate!<br/>

                        </div>
                    </li>

                    <li className="user-footer">

                        <div className="col-xs-12">
                            <Link className="btn btn-manageUser col-xs-12" to="/app/profile">Postavke profila</Link>
                            <br/>
                            <br/>
                            <Link className="btn btn-manageUser col-xs-12" to="/app/logout">Odjava</Link>

                        </div>
                    </li>
                </ul>
            </li>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

export default connect(mapStateToProps, { getUserProfile } )(UserMenu);
