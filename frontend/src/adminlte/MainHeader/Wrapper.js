import React from "react";


class Wrapper extends React.Component {
      constructor(props) {
      super(props);

      this.closeSidebar = this.closeSidebar.bind(this);

  }
    closeSidebar(){

      if(document.getElementsByClassName("dropdown user user-menu open")[0])
      {
          document.getElementsByClassName("dropdown user user-menu open")[0].classList.remove('open');
      }
    }
    render() {
        const {children} = this.props;



        return (
            <header className="main-header" onClick={this.closeSidebar}>
                {children}
            </header>
        );
    }
}

export default Wrapper;
