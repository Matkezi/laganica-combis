import React from "react";
import {Link} from "react-router-dom";


class Logo extends React.Component {
    render() {
        return (
             <Link to="/app/home" className="logo">
                <span className="logo-mini"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-2f924348a1185b4446235ba6e1977147.png" style={{width: '40px'}} /></span>
                <span className="logo-lg"><i>Krv je život</i></span>
            </Link>
        );
    }
}

export default Logo;
