import React from "react";
import {connect} from "react-redux";

import {contentWrapperMinHeight} from "./selectors";
import actions from "./actions";
import LoadingBar from 'react-redux-loading-bar'


class ContentWrapper extends React.Component {
  constructor(props) {
      super(props);
      this.closeSidebar = this.closeSidebar.bind(this);

  }

    closeSidebar(){
      if(!document.body.classList.toggle("sidebar-collapse")){
          document.body.classList.toggle("sidebar-collapse");
      }
      if(document.getElementsByClassName("dropdown user user-menu open")[0])
      {
          document.getElementsByClassName("dropdown user user-menu open")[0].classList.remove('open');
      }
    }

    render() {
        const {children, contentWrapperMinHeight, actions} = this.props;

        return (
            <div
                style={{"minHeight": contentWrapperMinHeight}}
                className="content-wrapper"
                onClick={this.closeSidebar}


            >
        <LoadingBar  />

                {children}
            </div>
        );
    }
}

export default connect(contentWrapperMinHeight)(ContentWrapper);
