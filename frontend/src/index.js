import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import App from "./app/layouts/App/App.js"
import store from "./app/configureStore";
import { authLogin } from "./app/actions/user/authActions";
import history from "./app/utils/historyUtils";
import {Router} from "react-router-dom";
import  "./app/stylesheets/index.less";
//eslint-disable-line no-unused-vars

const token = localStorage.getItem("token");
if (token) {
    store.dispatch(authLogin(token));
}


console.log(process.env.NODE_ENV);
ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>,document.getElementById("root"));