var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    devtool: "source-map",
    entry: {
        app: "./src/index",
        vendor: [
            "classnames", "history", "immutable", "lodash", "moment",
            "nprogress", "react", "react-bootstrap", "react-dom", "react-redux",
            "react-router", "redux", "redux-logger", "redux-thunk",
            "reselect", "superagent"
        ],
    },
    resolve: {
        modules: ["src", "node_modules"]
    },
    output: {
        path: path.join(__dirname, "../backend/static"),
        filename: "js/bundle.js",
    },
    plugins: [

        new webpack.optimize.CommonsChunkPlugin({name:"vendor", filename:"vendor.bundle.js",}),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new ExtractTextPlugin("stylesheets.css"),
        new webpack.DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("production")

        }),
        new webpack.optimize.UglifyJsPlugin({
            compressor: {
                warnings: false
            }
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ["babel-loader"],
                include: path.join(__dirname, "src")
            },
            {
                // expose immutable globally so we can use it in app.html
                test: require.resolve("immutable"),
                loader: "expose-loader?immutable"
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("css-loader?sourceMap-loader!less-loader?sourceMap")
            },

            {
                // move font files found within CSS to the build directory
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader?name=[path][name].[ext]?[hash]&context=./node_modules"
            },
            {
                // move images found within CSS to the build directory
                test: /\.(jpg|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader?name=[path][name].[ext]?[hash]&context=./node_modules"
            }
        ]
    }
};
