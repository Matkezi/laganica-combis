## Requirements

1. Git
2. Docker engine
3. Docker compose

## Set Up

1. Use git clone to pull this branch onto the server.
2. If you are going to dev, go to docker/web/web-entrypoint.sh and change npm run build to npm run start
3. Change to directory in which ``docker-compose.yml`` is located.
4. run ``docker-compose build``
5. run ``docker-compose up``
6. Once all is in place, elastic must be set a password via this call, it is called from the host (not from inside docker container) ``curl -XPUT -u elastic 'localhost:9200/_xpack/security/user/elastic/_password' -H "Content-Type: application/json" -d '{  "password": "matkosmithx"}'``
    - when elastic asks for password it's: changeme
7. Make sure to create a superuser account via shell by following these steps.
``docker ps`` and then ``docker exec -i -t <ContainerID that you get with docker ps> /bin/bash``
When you enter backend container:
    - python manage.py createsuperuser
    - exit continer
8. go to http://localhost:9000/admin, log in and find SITES.
    Change domain name and display name from example.com to localhost:9001 if you are dev (have npm run start in docker), otherwise put localhost:9000 (production) (important for emails and instagram callbacks on registration!)
9. If you are developing ( have npm run start in docker ) go to localhost:9001, otherwise go to localhost:9000 for frontend

## Update project on server

1. Git pull the new code.
2. If you changed only one container, build just that container without downtime with this:
    docker-compose up -d --no-deps --build <service_name>


## Notes

* Make sure you access everything with sudo. 
* Syntax for creating super user from shell is ``python manage.py createsuperuser``
* https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
* https://github.com/Seedstars/django-react-redux-base
