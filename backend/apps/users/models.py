from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # custom fields for user
    telephone = models.CharField(max_length=20, null=True)
    last_donated = models.DateField(null=True)
    last_called_for_donation = models.DateField(null=True)
    frequency = models.DecimalField(null=True, max_digits=5, decimal_places=5)
    distance = models.DecimalField(null=True, max_digits=5, decimal_places=5)
    pending_to_donate = models.BooleanField(default=False)
    company_email = models.EmailField(max_length=100, null=True)
    send_company_email = models.BooleanField(default=False)


    M = 'M'
    F = 'F'
    SEXES = (
        (M, 'M'),
        (F, 'F')
    )

    sex = models.CharField(choices=SEXES, max_length=20, null=True)
    

    Om = '0-'
    Op = '0+'
    Am = 'A-'
    Ap = 'A+'
    Bm = 'B-'
    Bp = 'B+'
    ABm = 'AB-'
    ABp = 'AB+'
    BLOOD_TYPES = (
        (Om, '0-'),
        (Op, '0+'),
        (Am, 'A-'),
        (Ap, 'A+'),
        (Bm, 'B-'),
        (Bp, 'B+'),
        (ABm, 'AB-'),
        (ABp, 'AB+'),
    )
    blood_type = models.CharField(choices=BLOOD_TYPES, max_length=20, null=True)
    


