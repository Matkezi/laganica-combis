from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class UserProfileConfig(AppConfig):
    name = 'users'
    verbose_name = _('User Profiles')

    def ready(self):
        import users.signals 