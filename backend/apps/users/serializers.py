from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer
from django.contrib.postgres.fields import ArrayField


class UserSerializer(UserDetailsSerializer):

    telephone = serializers.CharField(
        source="userprofile.telephone", allow_blank=True, required=False, allow_null=True)
    sex = serializers.CharField(
        source="userprofile.sex", allow_blank=True, required=False, allow_null=True)
    blood_type = serializers.CharField(
        source="userprofile.blood_type", allow_blank=True, required=False, allow_null=True)
    company_email = serializers.EmailField(
        source="userprofile.company_email", required=True)
    send_company_email = serializers.BooleanField(
        source="userprofile.send_company_email", required=True)

    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + \
            ('telephone', 'company_email', 'send_company_email', 'blood_type', 'sex')

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('userprofile', {})
        telephone = profile_data.get('telephone')
        send_company_email = profile_data.get('send_company_email')
        blood_type = profile_data.get('blood_type')
        sex = profile_data.get('sex')
        company_email = profile_data.get('company_email')
        instance = super(UserSerializer, self).update(instance, validated_data)
 
        # get and update user profile
        profile = instance.userprofile
        if profile_data:
            profile.telephone = telephone
            profile.company_email = company_email
            profile.blood_type = blood_type
            profile.sex = sex
            profile.save()
        return instance
