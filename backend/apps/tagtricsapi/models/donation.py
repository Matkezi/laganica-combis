from __future__ import unicode_literals
from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()

from django.contrib.postgres.fields import JSONField

class Donation(models.Model):
    donation_date = models.CharField(max_length=200, null=True)

    donor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='donor')
    questionnaire = JSONField(null=True)
    donation_made = models.BooleanField(default=False)
    location = models.CharField(max_length=200, null=True)