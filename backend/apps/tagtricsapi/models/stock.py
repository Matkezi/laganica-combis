from __future__ import unicode_literals
from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
from django.contrib.postgres.fields import ArrayField

class Stock(models.Model):
    owner = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    blood_max = models.IntegerField(null=True)
    blood_min = models.IntegerField(null=True)
    blood_opt = models.IntegerField(null=True)
    blood_current = models.IntegerField(null=True)

    Om = '0-'
    Op = '0+'
    Am = 'A-'
    Ap = 'A+'
    Bm = 'B-'
    Bp = 'B+'
    ABm = 'AB-'
    ABp = 'AB+'
    BLOOD_TYPES = (
        (Om, '0-'),
        (Op, '0+'),
        (Am, 'A-'),
        (Ap, 'A+'),
        (Bm, 'B-'),
        (Bp, 'B+'),
        (ABm, 'AB-'),
        (ABp, 'AB+'),
    )
    blood_type = ArrayField(
        models.CharField(choices=BLOOD_TYPES, max_length=20, null=True), null=True
    )

