from __future__ import unicode_literals
from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()

class UserSuggestion(models.Model):
    suggester = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='suggester')
    suggestion = models.TextField(null=True)


