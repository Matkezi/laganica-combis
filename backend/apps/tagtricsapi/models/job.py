from django.db import models
from djcelery.models import PeriodicTask, IntervalSchedule
import datetime
import json
from django.contrib.auth import get_user_model
User = get_user_model()


class Job(models.Model):
    CREATE_DATASET = 'create_dataset'
    UPDATE_DATASET = 'update_dataset'
    CREATE_FOLLOWING = 'create_following'
    CREATE_LIKING = 'follows_liking'
    CHECK_STOCK = 'check_stock'

    TYPES = (
        ('create_dataset', 'create_dataset'),
        ('update_dataset', 'update_dataset'),
    )

    PENDING = 'pending' 
    STARTED = 'started'
    FINISHED = 'finished'
    FAILED = 'failed'

    STATUSES = (
        ('pending', 'pending'),
        ('started', 'started'),
        ('finished', 'finished'),
        ('failed', 'failed'),
    )

    type = models.CharField(choices=TYPES, max_length=20)
    status = models.CharField(choices=STATUSES, max_length=20)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    trigger_at = models.DateTimeField(null=True)

    arguments = models.TextField()
    task_id = models.CharField(max_length=200, null=True)
    error_traceback = models.TextField(null=True)

    def save(self, *args, **kwargs):
        super(Job, self).save(*args, **kwargs)
        if self.status == 'pending' and self.task_id is None:
            from ..tasks import TASK_MAPPING
            task = TASK_MAPPING[self.type]
            return task.delay(job_id=self.id, arguments_json=self.arguments)

    periodic_task = models.ForeignKey(PeriodicTask, null=True)


    def schedule_every(self, period, every, args=None, kwargs=None):
        interval_schedules = IntervalSchedule.objects.filter(period=period, every=every)
        if interval_schedules: # just check if interval schedules exist like that already and reuse em
            interval_schedule = interval_schedules[0]
        else: # create a brand new interval schedule
            interval_schedule = IntervalSchedule()
            interval_schedule.every = every # should check to make sure this is a positive int
            interval_schedule.period = period
            interval_schedule.save()
        ptask = self.periodic_task
        ptask.interval = interval_schedule
        if args:
            ptask.args = args
        if kwargs:
            ptask.kwargs = kwargs
        ptask.save()
        self.save()

    def stop(self):
        """pauses the task"""
        ptask = self.periodic_task
        ptask.enabled = False
        ptask.save()

    def start(self):
        """starts the task"""
        ptask = self.periodic_task
        ptask.enabled = True
        ptask.save()

    def terminate(self):
        self.stop()
        ptask = self.periodic_task
        self.delete()
        ptask.delete()