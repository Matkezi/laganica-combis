from __future__ import absolute_import, unicode_literals
from backend.settings.celery import app
import json
from functools import wraps
from datetime import datetime
import traceback
from random import randint
from djcelery.models import IntervalSchedule

from .jobs.check_stock import *
from .models import Job 
   
def update_job(fn):
    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        job = Job.objects.get(id=kwargs["job_id"])
        job.status = Job.STARTED
        if job.type == Job.CREATE_LIKING:
            every = randint(15,50)
            period = 'seconds' 
            interval_schedules = IntervalSchedule.objects.filter(period=period, every=every)
            if interval_schedules:
                new_interval = interval_schedules[0]
            else:
                new_interval = IntervalSchedule()


                new_interval.every = every
                new_interval.period = period
                new_interval.save()
            job.periodic_task.interval = new_interval
            job.periodic_task.save()
        job.save()
        try:
            result = fn(self, *args, **kwargs)
            job.status = Job.FINISHED
            job.task_id = None
            job.save()
        except:
            traceback.print_exc()
            job.status = Job.FAILED
            job.task_id = None
            #Figure out a way to print it inside
            #job.error_traceback = traceback
            job.save()
    return wrapper


@app.task(bind=True)
@update_job
def check_stock_task(self, *args, **kwargs):
    task = CheckStock()
    task.run(kwargs["local_user"])


TASK_MAPPING = {
    'check_stock' : check_stock_task
}
