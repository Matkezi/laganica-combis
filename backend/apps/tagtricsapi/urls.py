from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    url(r'^donations/5', views.Donations.as_view(), name='donations'),
    url(r'^pending/5', views.Pending.as_view(), name='pending'),
    url(r'^check-stock', views.CheckStock.as_view(), name='check_stock'),
    url(r'^donate', views.Donate.as_view(), name='donate'),
    url(r'^donors/$', views.Donors.as_view(), name='donors'),
    url(r'^donation-confirmation/(?P<pk>[0-9]+)/$', views.DonationConfirmation.as_view(), name='donation_confirmation'),

    url(r'^$', RedirectView.as_view(url='classify_image/classify/')),


    
]