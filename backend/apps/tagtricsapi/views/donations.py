from ..models import Donation
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny
import json
from django.contrib.auth import get_user_model
User = get_user_model()


class Donations(APIView):
    parser_classes = (JSONParser,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def __init__(self):
        super().__init__()

    def get(self, request):
        result = {}
        user_id = 5
        user = User.objects.get(id=user_id)
        donations = Donation.objects.filter(donor=user)
        data = []
        for donation in donations:
            one_donation = {}
            one_donation['id'] = donation.id
            one_donation['location'] = donation.location
            one_donation['donation_date'] = "22.11.2014"
            one_donation['donation_made'] = donation.donation_made
            data.append(one_donation)
        result['count'] = len(donations)
        result['data'] = data
        return Response(result)

