from ..models import Donation
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny
import json
from django.contrib.auth import get_user_model
User = get_user_model()
from users.models import UserProfile

class Pending(APIView):
    parser_classes = (JSONParser,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def __init__(self):
        super().__init__()

    def get(self, request):
        result = {}
        user_id = 5
        profile = UserProfile.objects.get(id=1)
        if profile.pending_to_donate:
            result['pending'] = True
        else:
            result['pending'] = False
        return Response(result)

