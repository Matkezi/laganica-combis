from ..models import Donation
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny
import json
from django.contrib.auth import get_user_model
User = get_user_model()
from users.models import UserProfile


class Donors(APIView):
    parser_classes = (JSONParser,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def __init__(self):
        super().__init__()

    def get(self, request):
        result = {}
        profiles = UserProfile.objects.all()
        data = []
        for profile in profiles:
            one_profile = {}
            one_profile['first_name'] = profile.user.first_name
            one_profile['last_name'] = profile.user.last_name
            one_profile['sex'] = profile.sex
            one_profile['blood_type'] = profile.blood_type
            one_profile['last_donated'] = profile.last_donated
            data.append(one_profile)

        result['count'] = len(profiles)
        result['data'] = data
        return Response(result)

