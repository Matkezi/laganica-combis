

from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from datetime import datetime
from ..models import Job
from djcelery.models import PeriodicTask, IntervalSchedule
from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny


try:
    from django.utils import simplejson as json
except ImportError:
    import json


class CheckStock(APIView):
    parser_classes = (JSONParser,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def __init__(self):
        super().__init__()

    def get(self, request):
        local_user = str(request.user)
        all_following_jobs = Job.objects.filter(type=Job.CHECK_STOCK)
        for job in all_following_jobs:
            job_args = json.loads(job.arguments)
            local_user_username = job_args['local_user']
            if local_user_username == local_user:
                task_data = job.periodic_task
                from ..tasks import TASK_MAPPING
                task = TASK_MAPPING[Job.CHECK_STOCK]
                kwargs_json = json.loads(task_data.kwargs)
                job.task_id = task.delay(job_id=job.id, local_user=kwargs_json["local_user_id"])
                job.save()
                return Response(201)
            else:
                continue


        ptask_name = "%s_%s" % ('tagtricsapi.tasks.check_stock_task', datetime.now())
        ptask = PeriodicTask(name=ptask_name, task='tagtricsapi.tasks.check_stock_task')
        ptask.save()

        job = Job(periodic_task = ptask, type=Job.CHECK_STOCK)
        job_arguments = {}
        job_arguments['local_user'] = local_user
        job.arguments = json.dumps(job_arguments)    
        job.save()


        data = {}
        local_user_id = User.objects.get(username=local_user).id
        data['job_id'] = job.id
        data['local_user_id'] = local_user_id
        job.schedule_every('minutes', 10, kwargs=json.dumps(data))

        job.start()
        return Response(200)
