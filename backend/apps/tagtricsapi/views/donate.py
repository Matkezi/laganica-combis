

from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from datetime import datetime
from ..models import Job
from ..models import  Donation
from djcelery.models import PeriodicTask, IntervalSchedule
from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny

try:
    from django.utils import simplejson as json
except ImportError:
    import json

from users.models import UserProfile

class Donate(APIView):
    parser_classes = (JSONParser,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def __init__(self):
        super().__init__()

    def post(self, request):
        local_user = request.user
        profile = UserProfile.objects.get(id=1)
        profile.pending_to_donate = False
        profile.save()
        answer = request.data['answer']
        if answer == "Yes":
            donation = Donation()
            donation.donor = local_user
            donation.donation_made = False
            donation.save()
        return Response(200)
