from ..models import Donation
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated, AllowAny
import json
from django.contrib.auth import get_user_model
User = get_user_model()
from users.models import UserProfile
from ..models import Donation
import datetime

class DonationConfirmation(APIView):
    parser_classes = (JSONParser,)
    permission_classes = (AllowAny,)

    def __init__(self):
        super().__init__()

    def get(self, request, pk):
        donation_id = pk
        donation = Donation.objects.get(id=donation_id)
        donation.donation_made = True
        donation.location = 'Sveti Duh'
        donation.save()

        return Response(200)

