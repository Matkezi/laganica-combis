import datetime

from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from ..models import Stock
from rest_framework.views import APIView
from users.models import UserProfile


from django.contrib.auth import get_user_model
User = get_user_model()

import time
from django.utils import timezone
from django.core.mail import send_mail

try:
    from django.utils import simplejson as json
except ImportError:
    import json

class CheckStock(APIView):  
    parser_classes = (JSONParser,)

    def __init__(self):
        super().__init__()


    def sendNotif(self, stock):
        profile = UserProfile.objects.get(id=1)
        profile.pending_to_donate = True
        profile.save()

        send_mail(
            'Poziv na HITNO doniranje krvi',
            'Poštovani, pozivamo Vas na hitno doniranje krvi. Potvrdite na http://localhost:9001/app',
            'smithx.info@gmail.com',
            ['matko.zurak@gmail.com'],
            fail_silently=False,
        )
        # Ovdje ide poziv na njihov algoritam i vraća nam se lista IDeva kome šaljemo sve
        # ovdje ide email svima njima



    def run(self, local_user):
        current_stock = Stock.objects.filter(owner=local_user)
        #Check stock status end send notif if needed
        self.sendNotif(current_stock)

       