from django.conf.urls import url, include
from django.contrib.auth.views import login, logout_then_login
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view
from rest_framework import routers
from django.views.generic import TemplateView
from users.views import UserViewSet
from .views import app, index, privacy, smithxApp
from .facebook_login import *


router = routers.DefaultRouter()
schema_view = get_swagger_view(title='Pastebin API')
router.register(r'user', UserViewSet,)


urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('users.urls')),
    url(r'^swagger/$', schema_view),
    url(r'^smithx/', include('tagtricsapi.urls')),
    url(r'^app/', smithxApp, name='smithxApp'),
    url(r'^$', app, name='app'),

    # This is used for user reset password
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^accounts/', include('allauth.urls')),

    #web
        url(r'^privacy$', privacy, name='privacy'),
]
